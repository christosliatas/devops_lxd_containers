/* Launch a single named VM.  Then pull it's IP from lxc info and append it as a
property to the end of a properties file


TODO:  Modify this to update the DNS entry in local hosts

Sample Output From the LXC Command That we need to parse to obtain the
IP Address from the launched application
#####################################
#### lcx info command ###############
#####################################
work@lxddevops:~$ lxc info tomcat
Name: tomcat
Location: none
Remote: unix://
Architecture: x86_64
Created: 2018/11/21 02:32 UTC
Status: Running
Type: persistent
Profiles: default
Pid: 10742
Ips:
  eth0: inet    10.41.16.64     veth1L725X
  eth0: inet6   fd42:a09:55ba:c16a:216:3eff:fea6:a0cb   veth1L725X
  eth0: inet6   fe80::216:3eff:fea6:a0cb        veth1L725X
  lo:   inet    127.0.0.1
  lo:   inet6   ::1
Resources:
  Processes: 31
  CPU usage:
    CPU usage (in seconds): 7
  Memory usage:
    Memory (current): 48.36MB
    Memory (peak): 96.65MB
  Network usage:
    eth0:
      Bytes received: 375.17kB
      Bytes sent: 16.66kB
      Packets received: 242
      Packets sent: 233
    lo:
      Bytes received: 924B
      Bytes sent: 924B
      Packets received: 12
      Packets sent: 12

#########################
#### LIST COMMAND #######
#########################	  jwork@lxddevops:~$
jwork@lxddevops:~$ lxc list tomcat
+--------+---------+--------------------+----------------------------------------------+------------+-----------+
|  NAME  |  STATE  |        IPV4        |                     IPV6                     |    TYPE    | SNAPSHOTS |
+--------+---------+--------------------+----------------------------------------------+------------+-----------+
| tomcat | RUNNING | 10.41.16.64 (eth0) | fd42:a09:55ba:c16a:216:3eff:fea6:a0cb (eth0) | PERSISTENT | 0         |
+--------+---------+--------------------+----------------------------------------------+------------+-----------+

*/

package main

import (
	"fmt"
	"log"
	"os/exec"
)

// see: https://gobyexample.com/command-line-flags

//import "codesite.tld/authorName/Go-PublishingExample/epub"

//var dir = flag.String("dir", ".", "Directory to publish")

func GetImgIpAddress(imageName) {
	cmd := exec.Command("lxc", "info", "joetest")
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Error cmd.CombinedOutput(), err=", err, "stdoutStderr=", string(stdoutStderr))
		log.Fatal(err)
	}
	fmt.Printf("respText=%s\n", stdoutStderr)

}

func main() {
	fmt.Println("jlxc_launch_single")
	//flag.Parse()
	//epub.Publish(*dir)

	//cmd := exec.Command("ls", "-l")

    [] testStr = []byte('eth0: inet    10.41.16.64     veth1L725X ')

	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Error cmd.CombinedOutput(), err=", err, "stdoutStderr=", string(stdoutStderr))
		log.Fatal(err)
	}
	fmt.Printf("respText=%s\n", stdoutStderr)
}
