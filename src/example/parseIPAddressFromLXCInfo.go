/* Example parse out IP address from string returned by lxc info
 */

package main

import (
	"bytes"
	"fmt"
	"regexp"
)

// search for 0 or more spaces, followed by "eth" followed by 0 to 1 digit
// followed by at least one space.  followed by "inf:". followed by at least one space.
var lxcInfoRexExIPV4PrefixMatch = regexp.MustCompile(`\s?eth\d+:\s*inet\s*`)

// Pattern to validate that a parsed out IP address matches the general
// format 10.41.16.64
var ipV4ValidateFmt = regexp.MustCompile(`\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}`)

var contentLXCResp = []byte(`
Name: tomcat
Location: none
Remote: unix://
Architecture: x86_64
Created: 2018/11/21 02:32 UTC
Status: Running
Type: persistent
Profiles: default
Pid: 10742
Ips:
  eth0: inet    10.41.16.64     veth1L725X
  eth0: inet6   fd42:a09:55ba:c16a:216:3eff:fea6:a0cb   veth1L725X
  eth0: inet6   fe80::216:3eff:fea6:a0cb        veth1L725X
  lo:   inet    127.0.0.1
  lo:   inet6   ::1
Resources:
  Processes: 31
  CPU usage:
    CPU usage (in seconds): 7
  Memory usage:
    Memory (current): 48.36MB
    Memory (peak): 96.65MB
  Network usage:
    eth0:
      Bytes received: 375.17kB
      Bytes sent: 16.66kB
      Packets received: 242
      Packets sent: 233
    lo:
      Bytes received: 924B
      Bytes sent: 924B
      Packets received: 12
      Packets sent: 12
`)

func isValidCharInIPV4(c byte) bool {
	return c == '.' || (c >= '0' && c <= '9')
}

func isValidIPV4Format(tbytes []byte) bool {
	ipMatch := ipV4ValidateFmt.FindIndex(tbytes)
	if len(ipMatch) < 2 {
		return false
	}
	return true
}

// Parse IPV4 IP address out of lxc info status string.
// returns the string found
// error will be nill if a pattern that looks like
// an ip address was found.
func doParseIPV4(source []byte) (string, error) {
	var slen = len(source)
	loc := lxcInfoRexExIPV4PrefixMatch.FindIndex(source)
	if len(loc) < 2 {
		msg := fmt.Errorf("doParseIP No Match sourceLen=%d", slen)
		return "", msg
	}
	// Now copy digit or . bytes until we encounter
	// end of string or a character that could not be
	// part of a IP address.
	var buff bytes.Buffer
	for ndx := loc[1]; ndx < slen && isValidCharInIPV4(source[ndx]); ndx++ {
		buff.WriteByte(source[ndx])
	}
	tout := buff.String()
	if isValidIPV4Format(buff.Bytes()) == false {
		msg := fmt.Errorf("doParseIP No Match sourceLen=%d", slen)
		return tout, msg
	}
	return tout, nil
}

func main() {

	ipres1, err := doParseIPV4(content1Line)
	fmt.Println("parseIPAddressFromLXCInfo single line=", ipres1, " err=", err)

	ipres2, err := doParseIPV4(contentLXCResp)
	fmt.Println("parseIPAddressFromLXCInfo complete resp=", ipres2, " err=", err)

}
