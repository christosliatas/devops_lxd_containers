with open('/etc/tomcat8/server.xml', 'r') as myfile:
    data=myfile.read().replace('\n', '')

start = "Connector port=\"8080\""
repl = "Connector port=\"8083\""
newStr = data.replace(start,repl)

with open('/etc/tomcat8/server.xml', 'w') as myfile:
   myfile.write(newStr)