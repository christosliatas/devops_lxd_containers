# DevOps Automation  LXD & LXC  Containers  

LXD  based DevOps  Automation  for image creation,  image launch,  managing images across Machines.  It helps  DevOps deliver automated provisioning of workloads with fully automated infrastructure as code.    

LXD and our work planner allow  easy and incremental growth of compute capacity without expensive excessive planning.    Once you a host is added, tell the work planner what its IP address,  Availability zone and Security zone are the work planner will automatically start routing loads onto it. 

I found LXD compelling because it provides the foundational components to deliver complete environments with full load balancing and zero outage rolling releases without paying the cost of a full traditional virtualization environment.    LXD provides good  tools to allow a remnants strategy due to the way it treats process priority.   LXD is already compatible with open stack and can easily run in Ubuntu VM's in most virtual machines so it is relatively easy to deliver a zero bit change deployment infrastructure that can move between VMWare,  Openstack and the Cloud without change.   

LXD provides container isolation in trusted hosts while still providing the flexibility to route what we need out to the host interface.  This allows us to co-exist with existing networks by avoiding the temptation to take over the entire network stack.  This makes incremental deployment easier in traditional company data centers while remaining fully cloud compatible.   

Unlike many cloud environments you can start with LXD on one low powered host,  grow to 3 hosts for N+1 availability and then grow incrementally to thousands of hosts all without planning or paying for a large virtualized cluster such as VMWare.   It  provides the ultimate in flexibility to size or restrict containers without artificial limits such as 3 flavors.   LXD allows and may encourages many hosts with many different configurations rather than trying to artificially limit options to all nodes with the same configuration.  Our work planner supports this with the notion of availability zones so you can restrict placement as needed.  

### What lxd devops adds to LXC and LXD: 

*  A structured approach to building and re-using saved images.     Scripts to produce many common application images such as apache,  Haproxy,  tomcat, JDX, Kafka, zookeper, KV config store,  etc.  This also nicely isolates Image creation from image deployment to make auditing easy.
* Easily exposed layer 4 routing so the right ports from containers can be easily exposed on the host interface.  It remembers the dynamic IP assigned when the container is launched and maps the port automatically when the container is launched.
* Automatic configuration of layer 7 routing by URI prefix that  automatically routes work between hundreds of containers on many hosts.  It even remembers the layer 4 routes and builds the routing file automatically.
* A work planner that automatically deploys N containers for each requested template to a set of hosts based on current host load and capacity.   It  automatically handles deploying across the requested number of availability zones and can restrict deployment based on host security zone.   It can automatically start routing containers to new hosts within minutes of being informed of the hosts availability.
* A zero outage rolling deploy release manager that will automatically startup new nodes running the new version,  add them to the routing files and kill the old versions once their  connection count drops to zero. 
* Automatic support for zero bit change use of  remote KV configuration stores.  This allows rapid deploy of new environments with very little work other than telling the system which hosts it is allowed to use and how many containers to launch for each type of workload you desire.

> ** Please submit issues for enhancements and feedback

>
>
> Thanks,  [JOE Ellsworth ](https://www.linkedin.com/in/joe-ellsworth-68222/)
>
>

[TOC]

- 

# Main Files 

I divided the main documentation into two main sections to make them less confusing.    

-  **[DevOps with lxd conceptual guide](documentation/devops-with-lxd.md)** - explores the major principals such as load balancers,  image creation, proxy setup, etc and shows in detail how each concept can be realized.   I provide bash style code snippets that can be cut and pasted one step at time and so they can be combined easily for full automation scripts.
- **[DevOps lxd Automation](documentation/devops-lxd-automation.md)** -  Explains the conceptual approach and how to use the DevOps automation code we have written to fully automate to deliver "infrastructure as code" deployments where new environments can be deployed with minimal effort. 

### Other Important Files

* [Guiding Principals](documentation/guiding-principals.md) - The DevOps principles and project approach used in this project. 
* [LXD Container Commands Overview](documentation/lxd-commands-overview.md) - LXD sample commands I have captured and documented as I have been working on this project. 
* [file2consul](https://github.com/joeatbayes/file2consul) - Organizing Config Stores to minimize work & maintenance overhead
* [lxd reference & Research](documentation/lxd-reference.md) - LXD related links I found useful when doing research for his project.    
* [Reference & Research](documentation/reference.md) - General links I found useful during research for this project. 

# Tools & Commands

## 	Image Creation Scripts

* [make_all_img.sh](scripts/image/produce/make_all_img.sh) - Produce images by running all the image creation scripts listed below.   Not really all the images because some images like the layer7 haproxy can not be built until we know where other images have been deployed.
* [produce_updatedUbuntu.sh](scripts/image/produce/produce_updatedUbuntu.sh) -creates a ubuntu image from vendor supplied image.  Applies updates and cleans up.  This image acts as the source image for all other ubuntu server based images.
* [produce_basicApache.sh](scripts/image/produce/produce_basicApache.sh)- Creates a basic apache container with default content listening on port 80.
* [produce_basicJDK.sh](scripts/image/produce/produce_basicJDK.sh) - Creates a ubuntu image with the JDK loaded.  This is needed as foundation for many java based packages like tomcat. 
* [produce_basicTomcat.sh](scripts/image/produce/produce_basicTomcat.sh) - Creates a Tomcat container with support for user supplied WAR and JSP enabled.  Based on basicJDK since e needed the JDK to support JSP.  Loaded with sample WAR and listening on port 8080
* [produce_basicHaproxy.sh](scripts/image/produce/produce_basicHaproxy.sh) - Creates a HA proxy with configuration ready to load proxy config files. 
* [produce_hardenedUbuntu.sh](/scripts/image/produce/produce_hardenedUbuntu.sh) -  Place holder creates the hardened Ubuntu image but doesn't really do any hardening.  Only there as a place holder so we can wire things together.  All other scripts using Ubuntu server should use this template as foundation so they can inherit the hardening work when completed. 
* [apacheKV.sh](scripts/image/produce/apacheKV.sh) - Create an application specific Key Value server image.  Starts with basic Apache.   Unlike many image creation scripts this one requires that you supply the location of a Key Value directory structure it can use as content and the suffix you wish to use to name the new container image.
* [produce_apache_loaded_with_kv-sample.sh](example/kv-sample/produce_apache_loaded_with_kv-sample.sh). - Produces a Apache KV image loaded with application specific content.  
* [haproxy_with_config.sh](scripts/image/produce/haproxy_with_config.sh). - Produces a haproxy image to provide layer 7 load balancing for a series of containers previously mapped so they are available on the host interface.  It uses uri prefix such as /kv/ to route requests to the right containers on the right hosts.  The data generated is produced by  [launch_and_register.sh](scripts/deploy/launch_and_register.sh) and is converted into the haproxy syntax using [create_haproxy_routes.sh]   See also [launch_register_several_example.sh](scripts/deploy/launch_register_several_example.sh) and [haproxy_with_config_example.sh](scripts/image/produce/haproxy_with_config_example.sh)  This is where life gets a little complicated because we need to generate and map ports for almost every other container before we know what the content of the layer7 router will need to be. 

- [produce_basicKafka.sh](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/scripts/image/produce/kafka/produce_basicKafka.sh) - Produces a LXD image with a working version of a single node Kafka bundled with the required Zookeeper.
- [produce_basicOpenJDK8.sh](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/scripts/image/produce/produce_basicOpenJDK8.sh) - with the Open JDK 8 installed. Produces a LXD image from hardened ubuntu   Used by the Kafka and zookeeper containers.
- [produce_defaultJRE.sh](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/scripts/image/produce/produce_defaultJRE.sh) - Produce a container with current default version of open JRE (open JRE) installed from hardened ubuntu.  A good base for programs that need a Java runtime but no compiler.
- 

## Container Launch Scripts

- [launch_and_register.sh](scripts/deploy/launch_and_register.sh) -  Launch a named image and map required ports to host ports to allow external clients to reach the container on the specified port.  Also generates a mapping file recording which containers have which ports allocated on the host.   Parameters will dislay when ran with no parameters. 
- [launch_and_register_example.sh](scripts/deploy/launch_and_register_example.sh) - Example script showing the proper use of launch_and_register.sh to launch a single container and map 2 of  its ports.
- [launch_register_several_example.sh](scripts/deploy/launch_register_several_example.sh) - Example script showing how to use launch_and_register.sh to launch several containers each with their own port mappings.  This is an example of how the automated mapping and deploy system will deploy groups of containers to form an environment.

## Container Networking Scripts

- [wait_until_container_has_ip.sh](scripts/container/wait_until_container_has_ip.sh) - Loops checking to see if the named container has finished booting and received and IP.  This is needed because many apt get type commands will fail if ran with lxc exec before the IPV4 address is available. 
- [get_host_ip.sh](scripts/get_host_ip.sh) - returns host IP on primary ethernet address.  Needed this to support various scripts such as wiring together ports.
- [forward_port_host_to_container.sh](/scripts/container/forward_port_host_to_container.sh) - Forwards specified host port to the container using the lxd add device functionality.  This is required to allow a port on a container to be exposed outside of the host machine.    

## Utility Scripts

* [get_image_ipv4.sh](scripts/container/get_container_ipv4.sh) - Returns the number of cores available in current machine.  Used to determine how many concurrent worker processes to spawn when attempting to use all available CPU power but now cause a lot of thrashing.
* [get_image_ipv4.sh](scripts/container/get_container_ipv4.sh) - Parses the IPV4 address for the named container and returns it as a simple string that can be safely interpolated in a curl commands.  Useful since I prefer to let lxd assign the IP using default logic.   Also needed when we are assembling data to configure port forwarding and proxy. 
* [update_existing_packages.sh](/scripts/container/update_existing_packages.sh) - sends lxc commands to update packages on the named container.  This is used to ensure each image is running most recent packages and patches.  
* [utility_functions.sh](/scripts/utility_functions.sh) - common bash functions like removing duplicate space,  removing redundant /.   sourced into many other scripts.
* [get_number_of_cores.sh](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/scripts/get_number_of_cores.sh) Returns the number of cores in the current host.  

## Host Preparation Scripts

* [Ubuntu LXD prep](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/scripts/host_prep/basic_ubuntu_18_host_setup.sh) - Installs LXD and basic required tools to run and make the scripts in this repository
* [gigabyte BRIX setup Ubuntu 18.1 server](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/scripts/host_prep/gigabyte_brix_setup.sh)  Sets up basic configuration for the Gigabyte BRIX running Ubuntu 18.X series linux.    Includes configuring WiFi and connecting to a WEP2 access point. 
* 

## GO Tools Documented 

TODO: 

# Setup & Configuration

The easiest way to download the repository is to use git or the bitbucket web [download to copy the repository](https://bitbucket.org/joexdobs/devops_lxd_containers/downloads/) to your local disk.

```
git clone https://joexdobs@bitbucket.org/joexdobs/devops_lxd_containers.git
```

Most of the bash shell scripts will run just by making the working directory the location of where you saved the scripts.  They all use relative file access.  Simply use bash scriptname to run the scripts.   

```sh
# with the current working directory set to
# location where the shell script is saved 
# run the following from the shell prompt. 
bash produce_basicApache.sh
```

Examples of how to invoke or use the scripts are explained in [devops-lxd-automation](documentation/devops-lxd-automation.md).

The GO tools need be built.  They can be built locally using shell scripts supplied or you can use go get, go build and go install to fetch them directly from the repository. 

 That way you will receive the full set of examples, documentation and shell scripts.     An alternative is to use go get and let it download and build but you do not get the supporting files and documentation.

### Using the repository build script

Change Directory to same directory where you saved the repository. 

```
makego.sh
```

### Using the  Go Get tools

TODO: 

Setting up the  LXD host Container

- See: [First Time through lxd in lxd-commands.md](documentation/lxd-commands-overview.md#First+Time+through)
- See: [Getting LXD working in Ubuntu running in Virtual Box in lxd-commands.md](documentation/lxd-commands-overview.md#Getting LXD working in Ubuntu running in Virtual Box)

# Why did I do this work

I have made a significant investment in fully automated provisioning "infrastructure as code."      I find  it helpful to actually build similar components on a general purpose basis.  This helps ensure that I truly understand the work they are doing.  It also helps to ensure that I can provide effective guidance to the team  and make reasonable requests. 

In an era when business wants me to save money I must  consider alternatives different from  common  industry techniques that have  delivered current cost / ROI profiles.   This project (LXD-Devops) using LXD and LXC is one such alternative.  It appears to offer significant cost savings, higher availability,  more flexibility, reduced common failure points.  

##### Hyper converged infrastructure is expensive and delivers poor agility

I am disappointed with multi-million dollar costs to deploy hyper converged appliances and shared storage infrastructure.  This infrastructure has turned out to be expensive due large purchase cost along with extended approval cycles and extensive budget negotiations.  The size of the purchase results in extensive planning involving dozens of people over a more than a year.   I have seen this planning overhead  nearly double the stated cost of the hyper converged infrastructure.  The detailed planning delays delivery, reduces agility and velocity as we can burn time.  There is no combination of time saved by avoiding walking to touch compute appliances that have 7 year MTBF a few times per year that are likely to  pay off the cost of this planning overhead. 

##### A different approach is needed, start small, grow incrementally, deliver agility

Quite simply,  we need a different approach that allows us to start small,  grow in rational increments.   Deliver business value and spend more money as we can demonstrate the investment is warranted.  We need to allow this incremental growth in purchased capacity to grow without excessive planning and without a lot of manual effort to take advantage of it when it becomes available.  We need to do this without having to deprecate equipment just because it doesn't fit the next years hyper converged model.   lxd-devops uses LXD and LXC to deliver this capability.   

> If we can deploy a small fleet, quickly and inexpensively then expand it quickly and inexpensively.  A) It defers costs until business value can be demonstrated.    B) The total cost can be lower,  C) We can rapidly change plans to meet changing business priorities. 
>
> This is preferable to year long planning cycles where we absolutely must get right due to the senior executive visibility investments of this scale require. 

##### Distributed commodity,  better availability, lower cost, more performance

Rather than paying for expensive HA cluster equipment this repository can help you deliver,  Higher IOPS,  higher availability with better fault tolerance at dramatically lower cost by leveraging commodity hardware that can be customized for the load.   In one of our test labs we use Giga BRIX with 8GB RAM, 4 Cores, 1/2T of NVME and 1T of SSD for under 500 dollars or about 350 dollars per T.     The core system cost 150 dollars while the  RAM cost about 100 dollars so the cost for storage was 250 dollars or 168 dollars per T.  All of which is either fast SSD or very fast NVME.   A fully configured 10 node cluster costs under 5,000 dollars. 

Clients report prices ranging from [9,000 dollars per T](https://www.purestorage.com/content/dam/purestorage/pdf/NASPO/texas-pricelist.pdf) to over 13,000 per T for fast SSD storage.  Newer technology can deliver lower costs at 1500 USD per T by using RAM and SSD cached over spinning disk.  This cost model is similar to the cost per T for CEPH storage in a  hyper converged Openstack appliance but at that cost they are largely composed of spinning disk so it is not really comparable in performance. 

> One thing you seldom have architects complain about is IO performance when you deliver  NVME directly attached to the CPU on individual PCIE buses.  One thing architects always complain about is speed or unpredictability of speed from shared storage arrays.    Regardless of what storage specialists tell us,  It is a universal truth that direct attached storage especially NVME  is preferable  when building performance sensitive systems. 

##### Brix 9 to 53  times cheaper even if we count RAM and CPU as free

Our simple Brix based hardware cluster delivered a cost 9 times cheaper than the cheap storage and  53 times cheaper than the fast SSD based SAN.    If you add in the cost of planning and excruciating budgeting for multi-million dollar hyper converged appliance the combined compute + RAM + storage in this approach delivers a conservative 20 to 1 cost advantage.    

> To put this in perspective:  Adding 100T  to a fast SSD SAN at [9,000 USD per T](https://www.purestorage.com/content/dam/purestorage/pdf/NASPO/texas-pricelist.pdf)   would cost 900,000 USD.  For the same investment we could install 1,800 compute BRIX delivering 14,400 GB Ram, with 7,200 Cores and  2,700T of fast SSD or very fast NVME storage.     This cost advantage is before we add in the cost of RAM, CPU and Virtualization for a hyper converged system which could easily double the number of BRIX nodes we could deploy.

Our compute Brix configuration delivered the ultimate availability for distributed workloads but the approach could work just as well with larger rack mount units with hundreds of Gigabytes and dozens of storage devices that could easily push the cost advantage to over 30 to 1 while delivering the ability to run the largest work loads.     

##### Easier upgrade,  Fewer planned outages, lower risk of unplanned outage

Our simplified approach has a much lower probability of a shared component causing an outage and rolling deploys are easy compared to the month long planning cycle when  SAN  firmware needs to be upgraded.  The shared SAN array and hyper converged compute appliance delivers lower cumulative bandwidth have a higher risk of degradation due to congestion of common fiber channel links.  Why tolerate the risk of degradation and lower total performance when it costs less to avoid them.  The SAN array does provide some cool features such as snap clones and some data deduplication but it needs to deliver at least a 10 to 1 de-dup ratio before it even approaches comparable costs and even then it will not even come close to the cumulative bandwidth of hosts that have multiple NVME devices directly attached to the CPU. 

##### Cautions and Caveats

One thing that can derail this approach are software components that incur a per server cost.  This is one of the main hurtles  since many commercial packages and even Ubuntu have support models where server costs can be $2500 per server per year.   

These repeated tool stack costs can favor deploying a smaller number of larger commodity servers where we can concentrate hardware power relative to software stack licensing.  Some software licenses by the Core so it can deliver a better ROI to purchase a smaller number of the fastest possible cores.    

This is an area where LXD excels since it is fully capable of running larger multi-process complex applications in a single container.  LXD-Devops is also well suited to moving work between these servers based on observed load so the larger servers work well.   

Large servers concentrate processing power and storage where it can be consume in larger chunks so it would be a better fit for certain work loads like large RDBMS servers.   The ultimate decision would depend on the licensing model, software stack, support model and cumulative costs.   Either way LXD-Devops would transparently handle moving workloads between the servers based on load and capacity.

*  [NASPO Pure Storage Texas 2018 price list](https://www.purestorage.com/content/dam/purestorage/pdf/NASPO/texas-pricelist.pdf) - FA-X90R2-ETH-107TB-22/22-63/0-EMEZZ - 107TB X90R2 - 1,387,625 USD = 12,968 USD per T.  
* [Pure Storage 2017 US Price List](https://www.purestorage.com/content/dam/purestorage/pdf/NASPO/Pure_Storage_US_Pricelist_11-15-17.pdf) - FA-m70R2-ETH-208TB-38/38-22/22-22/22-22/22 - 208TB for 2,950,000 USD.  = 14,182 USD per T.
* 

------

###### TODO / Future

- TODO: [Monitoring on lxd](documentation/lxd-monitoring)
- TODO: [Go based config kv store](kv-go-scalable.md)
- TODO: lxd Layer 7 routing using haproxy
