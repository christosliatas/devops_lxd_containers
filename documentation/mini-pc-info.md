# Mini PC info 

I needed a server network with about 10 Linux servers to truly test my distributed lxc / lxd deployment scripts.  I also wanted to demonstrate a layered network along with the ability to demonstrate the random power failure.  Didn't want to spend a lot of money so decided to build network around a series of 4 core mini-pc.

## Why Mini PC

* Low power - 5 watt to 12 watt typical.  That means I can run 10 of them for about the same power budget as a single lightbulb.   This could be useful in a data center where power and cooling are major costs.
* Small - I have a small office and wanted keep the entire server array on my bookcase.
* high available - No moving parts or very few means very little to fail.
* Ideal for HA - share nothing architectures.  Allows us to remove the dependency on high end expensive shared SAN.  Because costs are low we can increase number of listeners cheap.
* Low cost - Being able to retire them cost effectively makes long term support easy just unplug.  Any single node failure is not a bit deal. Easy to run extra listeners.

## Mini PC Constraints

* Limited RAM.  Most of them come with 4 Gig,  A few are available in up to 16 GB.   The cost effective units under $200 generally cap out at 8 Gig. 
* Slow CPU.  Most in the sub $200 range run 2 to 8 times slower per core than data center grade CPU cores.

## My Constraints

* Low power - My office is rather small so I wanted 10 PC to run on little power in little space.
* Decided to stay Intel X64 compatible.  Could have had more options, lower cost with lower power in the ARM space but I wanted everything to move transparently into larger computers.

## Links to interesting parts.

* [Axium Ryzen 5 2400G, 16GB RAM, 500GB SSD, 4 core, 8 thread](https://www.amazon.com/dp/B07DRLH262/ref=psdc_13896591011_t2_B06XY6KWP1?th=1) - $629   [compared to J4105](https://cpu.userbenchmark.com/Compare/Intel-Celeron-J4105-vs-AMD-Ryzen-5-2400G/m444211vsm433194) - 85% faster single core speed,  60% faster quad core,  187% faster multi-core speed,  212% faster multi-core floating point. **(My favorite when building a real cluster)**
* [Intel BOXNUC7PJYH1](https://www.newegg.com/Product/Product.aspx?Item=N82E16856102204) $169 J5005 processor newer version of the [N42 and J4105](https://cpu.userbenchmark.com/Compare/Intel-Pentium-Silver-J5005-vs-Intel-Celeron-J4105/m487063vsm444211).  Still max memory of 12C.  No NVME space one 2.5" SATA space.
* [Hystou FMP03B rugged case](https://www.gearbest.com/tv-box-mini-pc/pp_635097.html?wid=1433363) I7 5500 dual core.  15W to 25W, Fanless with huge heat, No internal storage space, $295,  [compare U7 5500U vs J4105](https://cpu.userbenchmark.com/Compare/Intel-Core-i7-5500U-vs-Intel-Celeron-J4105/m22316vsm444211) - I7 has 3% faster single core speed.  J4105 has 11% faster quad core speed,  when over clocked I7 has 54% single core speed but equal quad core,  I7 max of 16GB, J4105 Max 8GB
* [MINIX NEO N42C Apollo Lake](https://cpu.userbenchmark.com/Compare/Intel-Pentium-N4200-vs-Intel-Celeron-J4105/m196248vsm444211) - $268 includes 4GB RAM expandable to 16GB.   Only has space for one M2 NVME with up to 512GB storage.  [compare to 4105](https://www.cpu-monkey.com/en/compare_cpu-intel_celeron_j4105-841-vs-intel_pentium_n4200-658)  - Slower than the 4105 CPU but confusing because spec chart shows max memory of 8GB total.
* [Gigabytes BRIX GB-BLCE-4105 mini / booksize](https://www.newegg.com/Product/Product.aspx?Item=N82E16856164115&ignorebbr=1) Nice system with 25% faster CPU than the Intel NUC6CAYH plus support for a  M2 NVME device which can provide even faster storage.  All for lower cost at $119 each.   Main complaint on this device is 8GB memory limit.  **(I am using 3 of these)**
* [Nice core speed comparison](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Celeron+J4105+%40+1.50GHz&id=3159)
* [AMD baming cube bare bones with 8 core about $300](https://www.amazon.com/Gigabyte-A8-5557M-Barebone-Components-GB-BXA8-5557/dp/B00VBNT3DU/ref=sr_1_1?s=electronics&ie=UTF8&qid=1542908691&sr=1-1&keywords=GB-BXA8G-8890)
* Another AMD mini PC](https://www.tinygreenpc.com/fanless-computers/micro-pcs/fitlet-rm/) no price on the website.
* [AMD server class 6 & 8 Core CPU](https://www.notebookcheck.net/First-AMD-Ryzen-fanless-mini-PCs-are-here.323836.0.html) - This is high end for mini PC strarting at 2 core $873 for 4 core.  May as well buy a small server at this cost range. 
* [Intel NUC kit with I7-8650U, 32GB RAM, 500GB NVME, SSD 1TB, Windows Home](https://www.amazon.com/Compact-Portable-i7-8650U-Business-Computer/dp/B06XY6KWP1) $1,179
* [Intel NUC Kit with I707567U, 4GB RAM, 32 GB NVME Optane, 2TB HDD, ](https://www.amazon.com/dp/B075VTD2W6/ref=psdc_13896591011_t4_B06XY6KWP1) $699
* [Amazon.com: CUK NUC Kit Mini Compact Portable PC (Intel i7-8650U, 32GB DDR4 RAM, 500GB NVMe SSD + 1TB HDD, Windows 10 Home) Tiny Business Desktop Computer: Computers & Accessories](https://www.amazon.com/Compact-Portable-i7-8650U-Business-Computer/dp/B06XY6KWP1)
* [I7 @ 2.7Ghtz boost to 4.5 , NUC817BEH 32GB Max RAM,  2ith 8GB Ram 128GB SSD $699](https://simplynuc.com/8i7beh-full/)
* [480 gb fips 140-2 Amazon.com: Silicon Power 480GB S60 MLC High Endurance SATA III 2.5" 7mm (0.28") Internal SSD Solid State Drive (SP480GBSS3S60S25AM): Computers & Accessories](https://www.amazon.com/Silicon-Power-Endurance-Internal-SP480GBSS3S60S25AM/dp/B06XRXSG95/ref=mp_s_a_1_23?ie=UTF8&qid=1544499211&sr=8-23&pi=AC_SX236_SY340_QL65&keywords=fips+140-2+2.5%22)
* [Amazon.com: Seagate Momentus Thin Hard Drive - Internal (ST500LT015): Computers & Accessories](<https://www.amazon.com/Seagate-Momentus-Thin-Hard-Drive-x/dp/B009T0FEY4/ref=mp_s_a_1_9?ie=UTF8&qid=1544498722&sr=8-9&pi=AC_SX236_SY340_FMwebp_QL65&keywords=fips+140-2+hard+drive&dpPl=1&dpID=413EUaGmwJL&ref=plSrch>)
* [Amazon.com: Buslink CipherShield CSE-16TRU3 16TB RAID 0 FIPS 140-2 256-bit AES USB 3.0/eSATA Hardware Encrypted External Desktop Hard Drive: Computers & Accessories](<https://www.amazon.com/Buslink-CipherShield-CSE-16TRU3-Hardware-Encrypted/dp/B00STZKNHI/ref=mp_s_a_1_16?ie=UTF8&qid=1544498722&sr=8-16&pi=AC_SX236_SY340_FMwebp_QL65&keywords=fips+140-2+hard+drive>)
* 




## Setup & Config

TODO:  