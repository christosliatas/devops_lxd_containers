# Important Links in this Project

* [devops-with-lxd](devops-with-lxd.md)  - Recommended reading shows in detail how most of the devops scripts we develop for automation are accomplished when done by hand.  Helps understand all the concepts we are using.

* [devops guiding principals](guiding-principals.md) Guiding principles recommended for all container based devops projects.

* [lxd commands overview](lxd-commands-overview.md) Notes I made when working through various tutorials. 

* [Setting up Ubuntu to support lxd](lxd-ubuntu-setup.md)  Steps we used from a generic ubuntu 18.10 to get lxd up and running. 

* [mini pc as servers info](mini-pc-info.md) - I used a set of mini PC about the size of books as my ubuntu servers for this project.  I am beginning to think that I could have some some projects millions of dollars with this approach.  They are the idea approach when using a shared nothing architecture because each node is cheap and easy to replace provided we have a devops automation approach that makes it easy.


[TOC]

# Reference & Links

## lxd Getting Started & Tutorial

- [Online LXD tutorial with interactive console](https://linuxcontainers.org/lxd/try-it/)
- [lxd 2.0 your first lxd container](https://blog.ubuntu.com/2016/03/22/lxd-2-0-your-first-lxd-container) very nice and fairly comprehensive introduction. 
- [lxd getting started docs - linux containers](https://linuxcontainers.org/lxd/getting-started-cli/)  - Using a remote LXD as an Image Server,   Manually Importing an Image,   Talking to a remote lxd host,  Basic intro to create, launch, stop, delete images.
- LXD online tutorial interacting with shell](https://linuxcontainers.org/lxd/try-it/) - Strongly recommended
- [LXD getting started from the command line](https://linuxcontainers.org/lxd/getting-started-cli/)  Deep documentation with details many other leave out. 
- [Ubuntu server guide lost of details about lxc](https://help.ubuntu.com/lts/serverguide/lxc.html) includes clone, user privileges, control groups, etc.
- How to setup and use LXD on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-lxd-on-ubuntu-16-04) - includes user setup,  lxd initialization,  create a container running nginx, explanation of how to forward ports. (some parts outdated)
- [All you need to know about working with containers](https://www.linuxjournal.com/content/everything-you-need-know-about-linux-containers-part-ii-working-linux-containers-lxc) includes coverage of attach,  console,   setting up users, etc.
- [LXC 2.x LXD Cheat sheet](https://www.jamescoyle.net/cheat-sheets/2540-lxc-2-x-lxd-cheat-sheet)
- [useful lxc command aliases](https://discuss.linuxcontainers.org/t/useful-lxc-command-aliases/2547/4)
- [Push files from host into container](https://discuss.linuxcontainers.org/t/how-do-i-copy-a-file-from-host-into-a-lxd-container/2066)

## Lxd Install & Get Started

- [Installing and configuring lxd](https://stgraber.org/2016/03/15/lxd-2-0-installing-and-configuring-lxd-212/)

- [lxd downloads](https://linuxcontainers.org/lxd/downloads/) 

- [read the docs lxd intro](https://lxd.readthedocs.io/en/latest/) includes instructions for build from source for both lxc and lxd.  Includes the apt get commands to install all the pre-requisites.  setup multi-host security,  migrating containers using LXD, bind home directory in a container,  Hacking with access via curl.  [Page 2](https://lxd.readthedocs.io/en/latest/api-extensions/)  Removing snapshots, Network management for LXD,  Storage Management via rest API,  Clustering,  Proxy configuration  [Page 4](https://lxd.readthedocs.io/en/latest/backup/) Backing up LXD container,  Full backup,  backup with lxc export to create external file tarball.  [creating and managing a cluster](https://lxd.readthedocs.io/en/latest/clustering/),  All nodes must have same storage pools,   [configuration storage](https://lxd.readthedocs.io/en/latest/configuration/),

- [Container configuration](https://lxd.readthedocs.io/en/latest/containers/)  Shows all the configuration settings used and then shows how to set them with lxc config command.  Includes things like network priority, limit for processes, limits disk priority, limits memory, memory swap priority,  cpu.priority,  Adding device, NIC / Network types.  MAS Integration - allows you to manage physical network and connect hosts to MAAS.  Disk Mount points,  Live Migration,  Snapshot configuration, [rest api through https](https://lxd.readthedocs.io/en/latest/debugging/),  [communication between hos and container](https://lxd.readthedocs.io/en/latest/dev-lxd/)  [lxd yaml config format](https://lxd.readthedocs.io/en/latest/preseed/) [production setup and common errors](https://lxd.readthedocs.io/en/latest/production-setup/)

  - The CPU limits are implemented through a mix of the cpuset and cpu CGroup controllers. limits.cpu resultsin CPU pinning through the cpuset controller. A set of CPUs (e.g. 1,2,3) or a CPU range (e.g. 0-3`) can be specified.
  - limits.cpu.priority is another knob which is used to compute that scheduler priority score when a number of containers sharing a set of CPUs have the same percentage of CPU assigned to them.
  - type NIC:  Physical - takes over host device,  bridged allows sharing same address mostly for port forwarding,  macvlan setup a new network device based on existing one using different MAC.  Macvlan can talk to the outside but not back to the host.   Allows to present multiple IP from single host, p2p -  creates virtual pair putting on side on container leaving other on host. 
  - [disk lxd](https://lxd.readthedocs.io/en/latest/containers/) Disk entries  are mountpoints inside the container. They can bind to existing file or directory on host or to a block device if one is available. [Network configuration](https://lxd.readthedocs.io/en/latest/networks/) 
  - [Profiles ](https://lxd.readthedocs.io/en/latest/profiles/) Profiles provide a container key/value set and any number of profiles can be applied to a container.
  - [lxd projects to split up your server](https://lxd.readthedocs.io/en/latest/projects/) LXD supports projects as a way to split your LXD server. Each project holds its own set of containers and may also have its own images and profiles.
  - [lxd Rest API Docs](https://lxd.readthedocs.io/en/latest/rest-api/)

## LXD Installing Specific Applications

* [Apache web server in lxd](https://www.linode.com/docs/applications/containers/access-an-apache-web-server-inside-a-lxd-container/)

## LXD API 

- [LXD REST API documenation]([https://github.com/lxc/lxd/blob/master/doc/rest-api.md)

- [Python API for access LXD](https://github.com/lxc/pylxd)

- [GO package to manage lxd clients](https://godoc.org/github.com/lxc/lxd/client)



## LXD Market 

- [5 reasons why we use LXD](https://openschoolsolutions.org/5-reasons-use-lxd/) - snapshots & migration,  adding a host,  copying files to a host, migrating a container to remote host.
- [Kata Containers Merger of LXD, HyperV and others](https://katacontainers.io/)
- [Ubuntu server support pricing](https://www.ubuntu.com/support/plans-and-pricing#ua-support) $1,500 per server per year to get FIP-certified cryptographic modules.
- [Understanding LXC and docker containers on Linux](https://community.oracle.com/docs/DOC-1002057)  Contains a nice comparison and where they can work together.  Also includes a nice list of lxc and docker commands to provide comparisons.

## LXC & LXD on Openstack

* [All-In-One Single LXC Container](https://docs.openstack.org/devstack/latest/guides/lxc.html) walks through the process of deploying OpenStack using devstack in an LXC container instead of a VM. 
* [Multi-node lab openstack ](https://docs.openstack.org/devstack/latest/guides/multinode-lab.html)
* [How to install LXD Container under KVM or Xen Virtual machine](https://www.cyberciti.biz/faq/how-to-install-lxd-container-under-kvm-or-xen-virtual-machine/)
* 

## lxc container monitoring

* [lxc container monitoring](https://wiki.opennms.org/wiki/LXC_container_monitoring)

## lxd Networking Port Forwarding

- [LXD: Five easy pieces](https://blog.ubuntu.com/2018/01/26/lxd-5-easy-pieces) - includes good description of bridged networking to make access easy. 
- [LXC netowrking configuration by Ubuntu](https://help.ubuntu.com/lts/serverguide/lxc.html#lxc-network)  Copious details on network configuration.   Giving containers persistent IP address.    Using IP Tables to forward ports.    Listing container IP addresses,    Auto start images,   Control groups for resource limiting, Cloning.
- [Simplifying port forwarding with LXD](https://ugle-z.no/articles/2016-09/simplifying-port-forwarding-with-LXD.html) - Nice script includes some nice scripting techniques.  Uses IP Tables rather than the build in functionality.  Unlike the build in functionality for add device this one can list all forward rules for the complete interface.
- 

## Networking  Network Interface Mapping

- [lxd proxy device](https://github.com/lxc/lxd/blob/master/doc/containers.md#type-proxy) Looks like a better way to support port forwarding lxc config device add <container> <device-name> proxy listen=<type>:<addr>:<port>[-<port>][,] connect=<type>:<addr>:<port> bind=<host/container> [example to configure port forwarding](

- [How to make LXD containers get IP address from LAN using macvlan](https://blog.simos.info/how-to-make-your-lxd-containers-get-ip-addresses-from-your-lan-using-a-bridge/)

### More Advanced Host Mapping (dnsmasq)

- [Advanced Dnsmasq Tips and Tricks](https://www.linux.com/learn/intro-to-linux/2018/2/advanced-dnsmasq-tips-and-tricks) - On Ubuntu linux  Dnsmasq allows us to provide local config of IP similar to what we used to do with localhosts.
- [HowTo dnsmasq](https://wiki.debian.org/HowTo/dnsmasq)
- [dnsmasq man page](http://manpages.ubuntu.com/manpages/bionic/man8/dnsmasq.8.html)

###### [Alternative DNS Looup using systemmd-resolve](https://discuss.linuxcontainers.org/t/access-lxd-guests-by-name-from-host/1632)

I have did not get this to work but it looks promising . 

```sh
# Lists my current hosts IP Address
ifconfig -a
```

```
# Take IP address in prior step and replace 10.x.x.1 
sudo systemd-resolve --interface lxdbr0 --set-dns 10.x.x.1 --set-domain lxd
```

### 

## Monitoring

* [Forwarding syslogd to a remote server](https://www.randomhacks.co.uk/how-to-configure-an-ubuntu-server-to-log-to-a-remote-syslog-server/)
* [Monitoring LXD with Netdata](https://docs.netdata.cloud/installer/)   [Long term storage of netdata logs](https://docs.netdata.cloud/backends/)
* [How to setup syslog-ng quickly for performance monitoring](https://www.syslog-ng.com/community/b/blog/posts/setup-syslog-ng-quickly-performance-monitoring-using-graphite-inside-docker)
* 
* 

## Local Host File Access

- [How to backup LXD containers](https://openschoolsolutions.org/how-to-backup-lxd-containers/) - lxc copy to move container to a remote host.  Display remote containers, display remote host containers,  lxc export to make a file that can be backed up as normal.  Backup with rclone.

- [Advanced LXC communicating between host and container](https://stgraber.org/2013/12/21/lxc-1-0-advanced-container-usage/)  includes reading files from the container.  Also includes mounting a host directory in either rw or ro mode from inside the container.  Also includes configuring lxc container to take over an interface from the host.

- [example of mounting host OS file](https://wiki.gentoo.org/wiki/LXD) 

  ```
   lxc config device add confexample sharedtmp disk path=/tmp source=/tmp/shared
   lxc start confexample
   lxc start confexample
  ```

- [lxc 1.0 storage](https://stgraber.org/2013/12/27/lxc-1-0-container-storage/) standard paths where lxc stores files. Including where to look for snapshots, etc.

- [Mounting your home directory in lxd](https://blog.ubuntu.com/2016/12/08/mounting-your-home-directory-in-lxd)  show how to allow access to host level files from inside a lxd container.
  - [A nicer way to mount your /home in LXD](https://tribaal.io/nicer-mounting-home-in-lxd.html)
  - [Linux containers as alternative to virtualbox for wordpress development](https://roots.io/linux-containers-lxd-as-an-alternative-to-virtualbox-for-wordpress-development/)

## Problem Resolution

- [identifying crashed lxc containers](https://atech.blog/viaduct/identifying-crashed-lxc-containers)  
  strace lxc-ls --fancy - should hang on bad image
   ps axjf -- shows process tree including lxc
- [claims the lxc-attach should work on ubuntu](http://manpages.ubuntu.com/manpages/xenial/man1/lxc-attach.1.html)
- [Getting started with lxc](https://linuxcontainers.org/lxc/getting-started/) - helped fix the missing lxc-attach command
- [How to create container in stopped state](How to create container in stopped state)
- 

# LXD Migration

* [Live migration using LXD compared to OpenVZ](https://www.diva-portal.org/smash/get/diva2:1052217/FULLTEXT02.pdf)  - Great conceptual paper nicely explains most of the important concepts.

# LXD on Windows & MAC

- [Native windows build for LXD Client](https://ci.appveyor.com/project/lxc/lxd/branch/master/artifacts)

- [LXD on Windows with binary and Macos](https://blog.ubuntu.com/2017/02/27/lxd-client-on-windows-and-macos)

## Windows SSH Client

- [bitwise ssh client](https://www.bitvise.com/ssh-client-download)  DevOps scripting quite often involved quite a bit of experimentation which also includes copying scripts over to the target servers.  I like to use the GUI editors on my laptops so a graphical SFTP client makes life easier.   Bitwise provides a terminal with quick and easy spawning of additional windows along with great emulation makes things a lot easier when testing new scripts.

- [command line usage of scp](https://haydenjames.io/linux-securely-copy-files-using-scp/) transfer files directly rather than using multiple checkin.

- Reset local git clone to overwrite your local changes   

  ```sh
  git reset --hard origin/master  
  git pull
  ```

# Unclassified #

- [Managing lxd containers from inside one of its containers](https://blog.simos.info/how-to-manage-lxd-from-within-one-of-its-containers/)
- [Docker inside of lxc containers](https://stgraber.org/2016/04/13/lxd-2-0-docker-in-lxd-712/) Ubuntu 16.04 and LXD 2.0, you can create containers for your users who will then be able to connect into them just like a normal Ubuntu system and then run Docker to install the services and applications they want.
- [Running Docker Swarm inside of LXC](http://andrea.corbellini.name/2016/04/13/docker-swarm-inside-lxc/)
- 



# Linux Related

* [Install squid caching proxy](https://help.ubuntu.com/lts/serverguide/squid.html.en) Need this to set local package cache to avoid re-download.

* [bash intro](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-7.html)

* [zsh manual](http://zsh.sourceforge.net/Doc/Release/)   - [writing scripts for zsh](http://www.linux-mag.com/id/1079/)- [zshell programming university](https://www.wisdomjobs.com/e-university/shell-scripting-tutorial-174/scripting-with-zsh-3404.html) alternative to bash with associative arrays.  [master your zsh](http://reasoniamhere.com/2014/01/11/outrageously-useful-tips-to-master-your-z-shell/)

* [linux cut with many examples](https://shapeshed.com/unix-cut/), [more cut examples](https://www.poftut.com/linux-cut-command-examples/) includes ability to return more than 1 field.

* [scp command examples](https://haydenjames.io/linux-securely-copy-files-using-scp/)

* [Add variable to path only if it does not exist](https://unix.stackexchange.com/questions/217622/add-path-to-path-if-not-already-in-path)

* [bash tell if file or directory exists](https://tecadmin.net/bash-shell-test-if-file-or-directory-exists/)

* [set global enviornment variable for everyone](https://askubuntu.com/questions/261760/setting-global-environment-variable-for-everyone)

* [add a file to /etc/profile.d and it should run at shell start](https://unix.stackexchange.com/questions/348295/ubuntu-16-04-scripts-in-etc-profile-d)

* [Run commands at system start or at service start](https://linuxconfig.org/how-to-automatically-execute-shell-script-at-startup-boot-on-systemd-linux)

* [Bash scripting for beginners](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)

* [Linux Tutorials](https://linuxconfig.org/linux-tutorials)

* [Grep command line with 11 examples](https://www.fastwebhost.in/blog/grep-command-in-unix-linux-with-11-simple-examples/)

* [positional parameters writing shell scripts](http://linuxcommand.org/lc3_wss0120.php)  Also good intro to shell scripts written with real functions.   [basic shell scripts](http://linuxcommand.org/lc3_writing_shell_scripts.php)  [control logic in bash](http://linuxcommand.org/lc3_wss0080.php)  [more control flow](http://linuxcommand.org/lc3_wss0110.php)

* [bash looping](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-7.html) plain text old style but well written

* [how to use 7zip](https://www.howtoforge.com/tutorial/how-to-install-and-use-7zip-file-archiver-on-ubuntu-linux/) - provides better compression than zip or gzip and provides option for 256 bit encryption at command line.  [encryptiing with 7zip](https://askubuntu.com/questions/928275/7z-command-line-with-highest-encryption-aes-256-encrypting-the-filenames)

* [ways to split string into an array](https://stackoverflow.com/questions/10586153/split-string-into-an-array-in-bash)

* [slitting arrays divided by lines](https://stackoverflow.com/questions/28417414/can-i-put-strings-divided-by-newlines-into-an-array-using-the-read-builtin)

* [Bash Concept Index](https://www.gnu.org/software/bash/manual/html_node/Concept-Index.html#Concept-Index)

* [input output redirection](https://robots.thoughtbot.com/input-output-redirection-in-the-shell), [how to print line to stderr in bash](https://www.systutorials.com/241726/how-to-print-a-line-to-stderr-and-stdout-in-bash/)

* [JSON on command line with jq tutorial](https://shapeshed.com/jq-json/) ----- [tutorial using a JSON API with js](https://stedolan.github.io/jq/tutorial/)  ------- [Convert JSON to YAML](https://www.commandlinefu.com/commands/view/12221/convert-json-to-yaml) Python utility  [convert YAML to json](https://www.commandlinefu.com/commands/view/12218/convert-yaml-to-json)   [bi directional json2yaml](https://www.npmjs.com/package/json2yaml)   [json manipulation on command line](https://shapeshed.com/jq-json/)  [command line yaml manip](https://github.com/mikefarah/yq)- sudo snap install yq -----  [python repository json to YAML](https://github.com/redsymbol/json2yaml)

* [complete guide to apt-get with great examples](https://help.ubuntu.com/community/AptGet/Howto)  Includes setting up a proxy to avoid redownload.

* [Show sockets open by a process](https://unix.stackexchange.com/questions/233490/show-network-connections-of-a-process)  We will need this when implementing rolling deploy to determine when we can close down a given container after all connections have been closed.   --- ss -nap | grep $(pidof firefox) --- netstat -p ---- netstat -tup ---- netstat -p | grep firefox | grep tcp  ----- lsof -i -a -p `pidof firefox`

* [Bash running multiple programs in parallel](https://stackoverflow.com/questions/3004811/how-do-you-run-multiple-programs-in-parallel-from-a-bash-script)

* [Reading A USB Drive]  

  ```sh
  # Look for USB drive already plugged in
  sudo fdisk -l  
  or  
  sudo lshw 
  
  #Make a mount point 
  sudo mkdir /media/usb
  
  #Mount the drive 
  sudo mount -t vfat /dev/sdb1 /media/usbstick 
  
  # Check to see if files show up 
  ls /media/usbstick
  
  # Unmount drive when done
  sudo umount /media/usbstick
  
  ```

* [Resize a logic volume](https://www.tecmint.com/extend-and-reduce-lvms-in-linux/)   - Walks through adding a new disk drive to a logical physical volume group and then using it to extend space available in a logical volume. 

  ```sh
  # Show basic information about physical volume
  sudo pvs
   
  # Show basic information about volume 
  # groups 
  sudo vgs
  
  # Show information about volume groups
  # I used this to see how much space was available
  sudo vgdisplay
  
  
  # Show information about logical volumes that currently exist
  sudo lvs
  
  # Add about 60GB from available physical space to
  # Logical volume. 
  sudo lvextend -l +18000 /dev/ubuntu-vg/ubuntu-lv
  
  # Resize the logical volume
  sudo resize2fs /dev/ubuntu-vg/ubuntu-lv
  
  # See if the additional space is added 
  df -h
  ```

* 



* [Reading lines from a file](https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash)



  ```bash
  echo "Start!"
  for p in (peptides.txt)
  do
      echo "${p}"
  done
  
  # OR
  
  while read p; do
    echo "$p"
  done <peptides.txt
  
  # OR
  while read -u 10 p; do
    ...
  done 10<peptides.txt
  
  # OR
  
  313
  down vote
  cat peptides.txt | while read line
  do
     # do something with $line here
  done
  
  
  # OR
  for word in $(cat peptides.txt); do echo $word; done
  
  # OR
  # ':' is the delimiter here, and there are three fields on each line in the file
  # IFS set below is restricted to the context of `read`, it doesn't affect any other code
  while IFS=: read -r field1 field2 field3; do
    # process the fields
    # if the line has less than three fields, the missing fields will be set to an empty string
    # if the line has more than three fields, `field3` will get all the values, including the third field plus the delimiter(s)
  done < input.txt
  
  # OR
  while read -u 3 -r line1 && read -u 4 -r line2; do
    # process the lines
    # note that the loop will end when we reach EOF on either of the files, because of the `&&`
  done 3< input1.txt 4< input2.txt
  
  ```

  [Bash builtin read and read array](https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#index-read)

  [Debugging with bash beginners guide](http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_02_03.html)

  [Setup a Open SSL certification authority CA on Ubuntu Server](https://networklessons.com/uncategorized/openssl-certification-authority-ca-ubuntu-server/)

# HAProxy Reference links

- [haproxy configuration manual](https://cbonte.github.io/haproxy-dconv/1.7/configuration.html)

- [performance tuning haproxy](https://blog.codeship.com/performance-tuning-haproxy/) maxcon setting can more than 10X TPS on same machine. 

- [haproxy proxy config](https://upcloud.com/community/tutorials/haproxy-load-balancer-ubuntu/)

- [ha proxy configuration files](https://cbonte.github.io/haproxy-dconv/1.7/configuration.html#2.1)

- [management docs test format very detailed](https://www.haproxy.org/download/1.6/doc/management.txt)

- [How to install HAProxy load balancer on Ubuntu](https://upcloud.com/community/tutorials/haproxy-load-balancer-ubuntu/)   explanation and samples of layer4 and layer7 load balancer along with basic config data.  Discusses various proxy algorithms.

- [haproxy management](https://www.haproxy.org/download/1.6/doc/management.txt)  -f <cfgfile> allows multiple config files to be referenced in order.  This allows us to add our layer 7 routing in a separate file while leaving the core haproxy config file untouched.

- [how to use haprox and set-up http-load-balancing on ubuntu](https://www.digitalocean.com/community/tutorials/how-to-use-haproxy-to-set-up-http-load-balancing-on-an-ubuntu-vps)

- [Mixing mode tcp and http - SSL termination and Passthrough - Help! - HAProxy community](https://discourse.haproxy.org/t/mixing-mode-tcp-and-http-ssl-termination-and-passthrough/2698)

- [Layer 4 Load Balancer for WordPress Application Servers on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-use-haproxy-as-a-layer-4-load-balancer-for-wordpress-application-servers-on-ubuntu-14-04) - DigitalOcean

- [install HaProxy on Ubuntu](https://help.ubuntu.com/community/HaProxy)

- [how to install and setup HAProxy on Ubuntu 16.04](https://www.techrepublic.com/article/how-to-install-and-setup-haproxy-on-ubuntu-16-04/) Includes a load balancer configuration.

- [HAproxy reloads with no lost connections](https://www.haproxy.com/blog/truly-seamless-reloads-with-haproxy-no-more-hacks/)

- 

  ## haproxy SSL Configuration

- [How to set up SSL passthrough with multiple domains with HAproxy](https://www.digitalocean.com/community/questions/how-to-set-up-ssl-passthrough-with-multiple-domains-with-haproxy)  DigitalOcean

- [Setup Your Own Certificate Authority (CA) on Linux and Use it in a Windows Environment](http://virtuallyhyper.com/2013/04/setup-your-own-certificate-authority-ca-on-linux-and-use-it-in-a-windows-environment/) - To support testing with SSL configurations.  We will want a way to configure the SSL in both haproxy and individual servers zero touch because otherwise it will not meet our zero touch deploy goals.  We will have to use a cert from a trusted authority our public facing proxy but the proxy to servers can use self signed certs.

- [Configure HAProxy to Load Balance Sites With SSL](http://virtuallyhyper.com/2013/05/configure-haproxy-to-load-balance-sites-with-ssl/) -  haproxy can load balance at layer 4 which means you can not look at the URI and just tunnel the SSL through to the end server.  You can not tunnel Layer 7 without terminating the SSL end point and re-encrypting for the next layer of connections you must decrypt traffic inside the request.   

- [HAproxy in the era of Microservices – 47 Ronin](http://47ron.in/blog/2015/10/23/haproxy-in-the-era-of-microservices.html) Great article that aligns well with my architectural thinking on devops in micro services era.

- [How we fine-tuned HAProxy to achieve 2,000,000 concurrent SSL connections](https://medium.freecodecamp.org/how-we-fine-tuned-haproxy-to-achieve-2-000-000-concurrent-ssl-connections-d017e61a4d27)

  ## haproxy dynamic configuration API

- [haproxy configuration with the haproxy runtime api](https://www.haproxy.com/blog/dynamic-configuration-haproxy-runtime-api/)

- [dynamic configuration of haproxy at runtime](https://www.haproxy.com/blog/dynamic-configuration-haproxy-runtime-api/)

- [vamp-router front and for haproxy dynamic config](https://github.com/magneticio/vamp-router/blob/master/README.md)   May make dynamically modifying HA proxy at runtime without an outage easier.  Written in GO.

- [Go library for interacting with HAProxy via command socket](https://github.com/bcicen/go-haproxy)

- [ha proxy runtime configuration API](https://www.haproxy.com/blog/dynamic-configuration-haproxy-runtime-api/)

- [HAProxy version 1.5.18 - Configuration Manual - disable server for maintenance to support rolling deploy.](https://cbonte.github.io/haproxy-dconv/configuration-1.5.html#9.2-disable%20server)



# ubuntu 18.04 & 18.1 server notes

* [Fix DNS Server in 18.04 using netplan](https://askubuntu.com/questions/1034912/netplan-nameservers-in-netplan-yaml-what-effect),  [How to setup name servers in ubuntu 18.04](https://www.techrepublic.com/article/how-to-set-dns-nameservers-in-ubuntu-server-18-04/)

* [Network Restart on ubuntu 18.04](https://linuxconfig.org/how-to-restart-network-on-ubuntu-18-04-bionic-beaver-linux) 

  ```sh
   sudo service network-manager restart	
   # OR
   sudo  systemctl restart NetworkManager.service
  ```

* Use the following if you are getting DNS name resolution errors but so you can not ping yahoo.com but could ping it directly with the IP address 

* ```sh
  #Edit /etc/netplan/50-cloud-init.yaml
  sudo vi /etc/netplan/50-cloud-init.yaml
  
  #So the file looks as follows
  network:
      ethernets:
          enp3s0:
              addresses: []
              dhcp4: true
              nameservers:
                      addresses: [64.233.217.2, 64.233.217.3]
                      addresses: [8.8.4.4, 8.8.8.8]
      version: 2
  
  # We  added the lines for nameservers and addresses inside of the cloud init yaml.  I think
  # we could place it in other file but have not 
  # tried.
   
  # Save file ran run the following:
  netplan apply
  ```

* [Show the list of drives in ubuntu.]  

  ```
   sudo lsblk
   # Will display each disk and each partition.
   # the exact path such as /dev/sda is needed
   # when trying to initialize storage for lxd 
   # using a block device. 
  ```

* [Fix wireless on Ubuntu 18.1 for Gigabyte BRIX  model#SGL-BLCE-4105 BWUS]   

  * sudo apt-get install wireless-tools 

  * [iwconfig](http://manpages.ubuntu.com/manpages/bionic/man8/iwconfig.8.html) - on my machine it showed wlps0 which was an available IEEE 802.11 ESSID

  * sudo apt-get install network-manager  - Needed to list the connection list. 

  * [wpa/wpa2/ suplicant for signing onto wifi with web](http://w1.fi/wpa_supplicant/)

  * [ubuntu commands to configure wifi](https://docs.ubuntu.com/core/en/stacks/network/network-manager/docs/configure-wifi-connections) 

  * To get the Wireless configured and working on my brix 

    ```sh
    sudo nmcli d wifi connect AccessPointName password MYWEBPASSWORD
    ```

  * To list the MAC address of the access point connected to 

    ```
    nmcli -f BSSID,ACTIVE dev wifi list | awk '$2 ~ /yes/ {print $1}'
    ```

  * . To get MAC address of the Wifi adapter in the

    ```
     linux box nmcli | grep wifi 
    ```

  * To Get the IP address of the wireless adapter  

    ```
    nmcli | grep inet4 
    ```

  * . inet4 192.168.0.20/24

  * .wifi (iwlwifi), 7C:76:35:FF:D0:CA, hw, mtu 1500

  * .

  * .

  * .

  * 





# Tools

## Editors

I am not a great fan of IDE.  I hate to see extra artifacts created by editors and I hate anything that encourages developers to not build and test in the environment where they will run their code.  Just too many expensive problems like 100 person development teams in billion dollar companies who loose days every time they switch domains because they got hooked into IDE specific configurations and failed to maintain build from script.   On the other hand limited code suggestions,  search files, syntax coloring are useful and can make programing easier.    I still tend to run my tests from the command line and look at compiler errors in a command line because I don't like to give my code a chance to crash the editor or visa-versa.   Most of the highest productivity developers I have encounters use similar practices.     There are many stand by editors such as vim, nano and the big expensive IDE but they are well described elsewhere.  I have delivered million $ systems with a subset of these editors and never had to pay for a IDE.  At the end of the time I also had great build scripts with everything in source assets.  Great build scripts not accidental dependencies on editors  has saved my customers a lot of time and money. I know this because I end up unwinding this dependency where teams have gotten stuck in 2 year old IDE in just about every company. It is always such a large time and velocity killer that it can not possibly be paid back by the savings early in the life cycle.

* [JOCKO - Go based Kafka wiithout zookeeper dependency](https://github.com/travisjeffery/jocko)
* [scintilla and SciTe](https://www.scintilla.org/SciTEDownload.html) One of the editors I come back to over and over. It has one of the best indent formatting functions I have ever seen.  Available on both Linux and Windows.   Great support for colorization,  optional on screen line numbering with really helps when manually jumping to next error.   and one of the best visual indent guides I have ever seen.
* [kate editor.org/](https://kate-editor.org/) Good Syntax editing. Good static wrapping. Menus are ugly but left hand list of open files is handy.   Available on both Linux and Windows.  I like the limited auto hints but like most IDE they are of limited value.  Lots to like about this editor but it fails to recognize files that have been changed elsewhere and a warn the user.
* [notepad++](https://notepad-plus-plus.org/) One of my favorites and probably my most heavily used editor.  Has great plugins for JSON formatting and just about anything else you want to do.  Not good for files over 5MB.  Mostly windows but I have recently seen Linux builds.
* [LiteIDE](file:C:/Go/liteide/share/liteide/welcome/en/website.md)  Go Centric but has decent colorization, code folding and limited code hints.  I find myself returning to this editor when editing GO.
* [Idle python](https://docs.python.org/3.4/library/idle.html) - The Idle editor is truly bare bones but it has some of the best auto indenting available for python.  It also provide some of the best colorization.  It is easy to crash when using as debugger but as long as you edit in Idle and run from command line it is remarkably stable.   With all the great editors and IDE out there Idle's approach of one file, one window makes it much easier to edit many files simultaneously especially on large format  4K monitors.   
* [Programmers File Editor](https://www.lancaster.ac.uk/staff/steveb/cpaap/pfe/) Fast, small and deals with larger files better than notepad ++.  Windows only.   No colorization.   If your computer is small or slow then this is the editor for you.
* [Typora](https://typora.io/) -  Wsywig markdown editor.  I find this a addictive tool since it frees me up to write with less worry about syntax.    The markdown syntax it uses is not 100% compatible with bitbucket but it is close enough, most of the time.  A great time saver that encourages programmers to keep their .md files in the directory with their code so  it comes up nicely formatted in the web browser access to their repository.

## SSH & Communications

* [bitvise](https://www.bitvise.com/ssh-client-download)  -  This tool supports really fast write, test, debug cycles when all you have is a ssh window for access.   This is a tool that you never knew you needed until you use it and then hate being without it.    Since I like to edit on my laptop and run on remote Linux servers it is useful to have a very easy way so I can launch more terminal windows with a single click that is tolerant of sporadic connection losses.  It is also nice to have graphical file copy so I can rapidly execute the change code,  move code, execute and cycle again.   I execute this cycles dozens of times per hour when exploring new concepts.  Fast cycle times is  one reason I strive to have really fast builds and will structure my projects to deliver them.   One approach is to use the git clone, make a change and then a git pull but when dealing with dozens of times per hour this can pollute the git history so I prefer to clone initially then when testing micro changes I use this tool to transfer files from the directories I am active in.   Once I reach a commit point with semi-stable code I commit and then hard-reset and return to origin in my remote and bring it up to date with a pull.  I never use this approach to deliver scripts to production systems.  I periodically (3 times a week) wipe the remote linux config by killing that lxd container and rebuild from scratch using a script.  This ensures that I have not become accidentally dependent on a side effect of something I did manually.
* 