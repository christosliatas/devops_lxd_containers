# Setup lxd/lxc with a New Ubuntu

Installs required packages on the ubuntu host and gets it ready to run the larger scripts.

```sh
#Update local packages
sudo apt update
```

```sh
#Install lxd
sudo apt-get install lxc --assume-yes
sudo apt install lxd --assume-yes
```

```sh
#Add your user to lxd group
sudo adduser lxd
sudo usermod --append --groups lxd YOURUSERNAME
newgrp lxd

# Edit to replace YOURUSERNAME with the username you used
# to login or you will have to use sudo for every command.

```

```sh
# If you see error message:
#   Failed container creation: No storage pool found. Please create a new storage pool  after a lxc launchy command then 
# Initialize lxc to setup storage
lxd init
# These are the answers I used which creates the most 
# simple but also the slowest lcd storage which uses a
# file in the vm.
Would you like to use LXD clustering? (yes/no) [default=no]:
Do you want to configure a new storage pool? (yes/no) [default=yes]:
Name of the new storage pool [default=default]:
Name of the storage backend to use (btrfs, ceph, dir, lvm, zfs) [default=zfs]: dir
Would you like to connect to a MAAS server? (yes/no) [default=no]:
Would you like to create a new local network bridge? (yes/no) [default=yes]:
What should the new bridge be called? [default=lxdbr0]:
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
Would you like LXD to be available over the network? (yes/no) [default=no]: yes
Address to bind LXD to (not including port) [default=all]:
Port to bind LXD to [default=8443]:
Trust password for new clients:
Again:
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: yes

# Here is the YAML file generated which can be used as a parameter for a future run.
config:
  core.https_address: '[::]:8443'
  core.trust_password: YOURPASSWORD
networks:
- config:
    ipv4.address: auto
    ipv6.address: auto
  description: ""
  managed: false
  name: lxdbr0
  type: ""
storage_pools:
- config: {}
  description: ""
  name: default
  driver: dir
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr0
      type: nic
    root:
      path: /
      pool: default
      type: disk
  name: default
cluster: null
```



```sh
# Add Python, GOlang and .git
# We use these for various utilities in the included scripts
# by using GOLang we can build utility configurators on
# host copy the executable into client and run without
# installing extra runtimes.
sudo apt-get install python --assume-yes
sudo apt-get install golang --assume-yes
sudo apt-get install git-core --assume-yes
```

- [Configuring LXD digitalocean](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-lxd-on-ubuntu-16-04)
- [Configuring a Static IP on Ubuntu 18.04 server](https://askubuntu.com/questions/1029531/how-to-setup-a-static-ip-on-ubuntu-18-04-server) Ubuntu has changed the tried and true so it now requires editing a Yaml file.  [article #2](https://linuxconfig.org/how-to-configure-static-ip-address-on-ubuntu-18-04-bionic-beaver-linux)   [article #3](https://websiteforstudents.com/configure-static-ip-addresses-on-ubuntu-18-04-beta/) 
- [How to configure static IP address on Ubuntu 18.10 Cosmic Cuttlefish Linux](https://linuxconfig.org/how-to-configure-static-ip-address-on-ubuntu-18-10-cosmic-cuttlefish-linux)
- [Assign multiple IP addresses to single Network card in DEB based systems](https://www.ostechnix.com/how-to-assign-multiple-ip-addresses-to-single-network-card-in-linux/)  this one is for Ubuntu 16.04 and the approach has changed in newer versions.



- 