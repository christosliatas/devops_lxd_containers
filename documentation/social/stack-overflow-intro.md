Take a look at my open source project on bitbucket [devops_lxd_containers](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/) It includes:

- Scripts to build lxd image templates including Apache, tomcat, haproxy.
- Scripts to demonstrate custom application image builds such as Apache hosting and key/value content and haproxy configured as a router.
- Code to launch the containers and map ports so they are accessible to the larger network
- Code to configure haproxy as layer 7 proxy to route http requests between boxes and containers based on uri prefix routing. Based on where it previously deployed and mapped ports.
- At the higher level it accepts a data drive spec and will deploy an entire environment compose of many containers spread across many hosts and hook them all up to act as a cohesive whole via a layer 7 proxy.
- Extensive documentation showing [how I accomplished each major step](https://bitbucket.org/joexdobs/devops_lxd_containers/src/master/documentation/devops-with-lxd.md) using code snippets before automating.
- Code to support zero-outage upgrades using the layer7 ability to gracefully bleed off old connections while accepting new connections at the new layer.

The entire system is built on the premise that image building is best done in layers. We build a updated Ubuntu image. From it we build a hardened Ubuntu image. From it we build a basic Apache image. From it we build an application specific image like our apacheKV sample. The goal is to never rebuild any more than once and to re-use the common functionality such as the basicJDK as the source for all JDK dependent images so we can avoid having duplicate code in any location. I have strived to keep Image or template creation completely separate from deployment and port mapping. The exception is that I could not complete creation of the layer 7 routing image until we knew everything about how other images would be mapped.

[Posted on stack overflow on Dec-22-2018](https://stackoverflow.com/questions/44456522/how-to-automate-application-deployment-when-using-lxd-containers/53580941#53580941)