# DevOps lxd & lcx Automation 



I broke automating DevOps into two main sections because  mixing the two was confused readers who had separate interests. 

*  **[DevOps with lxd conceptual guide](devops-with-lxd.md)** - explores the major principals such as load balancers,  image creation, proxy setup, etc and shows in detail how each concept can be realized.   I provide bash style code snippets that can be cut and pasted one step at time and so they can be combined easily for full automation scripts.

* **This page** -  Explains the conceptual approach and how to use the DevOps automation code we have written to fully automate to deliver "infrastructure as code" deployments where new environments can be deployed with minimal effort. 


## Table of Contents

[TOC]

# Summary

Please read [DevOps with lxd concepts and examples](devops-with-lxd.md) before running the scripts in this guide.  We show in detail how many of the things we have automated are accomplished.  This background is helpful when using the automated tools.  

## Goals

Read our **[DevOps Guiding Principals](guiding-principals)** - for a an in-depth discussion of goals and principals.  These guiding principals and design guidelines were used to guide the design of the DevOps automation code in this repository.

## Conceptual  Approach

We can break down the basic approach into the following discrete steps.  Care is taken to keep each of these steps discrete from prior steps whenever possible.  We attempt to keep all scripts isolated so one phase can be ran in isolation of the others to simplify testing and to stay true to single bit deploy.

* **CI/CD Building the basic artifacts** - WAR files or other deployable artifacts. (out of scope for this document)
* **Mapping hardware host images servers into an environment**.   Includes install and patching of the host OS.   This can be deferred and done on multiple lxc containers acting like separate hosts. Knowing the target design is required to publish the files the automation system uses to deploy the container images.
* **Building container images** - This includes building the lxc published images in layers.
* **Deploying container images** - Consumes the container images built in the prior stage and deploys or launches them to become operational images.   This also includes hooking them into load balancer and port forwarding as needed.    
* **Connecting to Operational Infrastructure** - Includes configuring edge firewalls,   hardware load balancers, etc to deliver the traffic to our deployed environments. 
* **Supporting version releases** - This generally uses the steps building container images and  deploying container images but can leak into connecting to operational infrastructure and operational support especially during major releases when a outage may be required.
* **Operational of running environment** - Support for viewing operational services and their containers,  detecting failed images, restarting services as needed. 

There are some overlaps between stages but we attempt to keep them clean where ever possible.    An example is that we need to know about some of the deployment targets when building the haproxy container because it needs to know where the individual containers are at when building the proxy config.  Even here we build the haproxy container with that knowledge and the deploy it as part of the deploy container images step.

### Building Container images

When building container images we start with a simple script that will produce a named and image that is made available to launch using the lxc publish commands.     

#### Staged building of published images

To maximize our use of the single bit deploy principle we create our containers in stages.  Each stage will produce a somewhat more specialized container for An example of this for a basic Tomcat server is:

1. The basic image would be  "updatedUbuntu" which includes a baseline ubuntu image that has been patched and updated by [produce_updatedUbuntu.sh](../scripts/image/produce/produce_updatedUbuntu.sh)
2. Consumes "updatedUbuntu" and publishes "hardenedUbuntu" after it has applied appropriate changes to make it fully hardened to comply with security requirements.  See [produce_hardenedUbuntu.sh](../scripts/image/produce/produce_hardenedUbuntu.sh)
3. Consumes "hardened Ubuntu" and publish "basic JDK".  See [produce_basicJDK.sh](../scripts/image/produce/produce_basicJDK.sh)
4. Consumes "hardenedUbuntu"and publish "basicTomcat" which includes enough for tomcat to respond with it's default page on port 8080. See [produce_basicTomcat.sh](../scripts/image/produce/produce_basicTomcat.sh)
5. Consumes "basicTomcat" and publishes "hardenedTomcat" which includes enough hardening specific to tomcat to security requirements.
6. Consumes "hardenedTomcat" and publishes a application specific version such as "bloggingService"  It already has a hardened tomcat so all it needs to do is add the appropriate WAR files, modify the tomcat configuration as needed and add any static content files.  

#### Supporting other runtimes

This same basic process would be used for each process but if you had a GO based KV store you would start from "hardenedUbuntu" and then customize.     For the next Java service they only need to start with "hardenedTomcat" and customize with the specific needed war files.      For each of these images we also apply resource limiting commands to keep them from consuming the entirety of the host system resources.

#### Multiple Java services in a single container

In the case of a Java based app server it is reasonable to start with a "hardenedTomcat" and customize it with many WAR files. The total set of WAR files for any target app server should be easy to customize so we can change the grouping at will in the future.   This is critical to allow us to customize our application to hardware mapping as heavy resource consumers are recognized.  Specifying the grouping of the WAR files will be covered as part of detailed scripting.

#### Security Scans:

In a full deployment we would likely run a security scan on each layer of image as produced so we can  log it's compliance.   In an ideal scenario security will accept this as a complete scan and if they need to do future scans we would provide support to launch the saved image and allow them to scan it at will.  If they find and defects we fix it back in the image provisioning scripts and redeploy to replace the running defective containers.

#### Early testing of end to end container creation

Since we are simply using labeled images we can make some simplifying assumptions early in the process where we  consume our  "updatedUbuntu" and publish "hardenedUbuntu" even without the applying the actual hardening steps.  

We can actually create are "produceHardenedUbuntu" script that simply does the re-publish.  This provides us a baseline image to use when building the downstream images  and allows us to produce the complete set of images we need to test Deploying container images.    

We could not release in production until the actual hardening is done but nothing else in the pipeline should change.   We would then comeback and fill in the details to produce the actual hardened image when time and resources allow. 

The same approach can be used to produce other hardened images such as "hardenedTomcat" so we can focus on producing specific value add of a Tomcat that includes the War files and content we needed.

#### Supporting the Chaining of Images

To support a generic process where each script consumes a published image and produces a new image we will support a semantic where each script receives or knows what hardened image it needs and what script would produce that image.   

If the published image is missing it can be produced by chaining out to the script that can create it and then continue on.    This can work all the way to the top of the image stack ultimately producing the basic Ubuntu image.

Details of the specification of this chaining relationship is shown in the detailed sections below. 

#### Intentional Generic Naming of Images

The name "hardenedUbuntu" was chosen explicitly over "hardenedUbuntu18.10" and "basicTomcat" over "basicTomcat8.5" because I am a proponent of allowing easy upgrade to most recent versions through the stack wherever possible.   

The design intent is to allow the script "produceHardenedUbuntu" to be upgraded  to work with the next version of Ubuntu at any time and all the other scripts would simply inherit the new version the next time we build images.   This is equivalent to building with the #latestStable tag for all images in the system.

It is viewed as desirable to discover things that have broken in each release early during the release process and fix them when they are still relatively easy.   This is intended to address the anti-pattern of explicit versioning where once teams name a version they stay with that version for years missing all the patches and improvements.  This creates accumulating technical debt, increases security risks and drives down velocity.   Following the anti pattern may seem to save time but it is only a short term savings that gets progressively more expensive to remediate.

It is possible to use a named version instance when upgrade is simply impossible but it may require creating version named produceXX scripts all the way up the tree to where the versioned artifact is needed.  This should be avoided whenever possible and when needed it should only be allowed with Director or VP level approval because it will have a long term cost impact.

#### Using Configuration Store values during Image Creation

We are serious about applying the zeroBit deploy principal even to devops scripts.   this means when we are building artifacts we pull values from our configuration store to help make decisions.   For example in our  "produceUpdatedUbuntu" script we need to know which Ubuntu version to use.    In lxc this maps back to a specific image we pull from Ubuntu to launch.  This image should be defined in the configuration store and used by the script. 

In some instances it is easier to have all config store data available for bash or sh scripts to consume in a easily accessible format so we have extended file2consul to provide file2env tool which saves the configuration values as environment variables.    

There should be no environment specific values needed at this stage so we only need to populate from the ImageCreation  portion of the configuration store key space.

We should see no hardcoded machine names or IP addresses in any script or property.   Even the machine to use as the configuration store should be passed in as a parameter. 

#### Simple Tests during Image Creation

When creating images it is best to be sure the basic image is able to perform the necessary functions.      Some services can not do their full job without connecting to downstream resources but they should always be able to respond to a health check that returns the set of dependent resources that are currently unavailable.   

We use this simple healthcheck capability to determine that the service has all the right components and could operate if all the downstream dependencies are available.   If the running image can not pass this healthcheck then the image will not be published.   In some instances like basic Ubuntu we may not be able to supply a health check but we should at least be able to ping the container to be sure it is running. 

##### Graceful wait for downstream resources

Each service must be able to wait gracefully for available it's downstream resources to become available and to recover nearly instantly once they do become available.    Configuration values in the configuration store can be changed at any time and the service must accommodate those changes.  The configuration store URI can change and the service must accommodate that change. 

#### Containers start with running services

The container image is saved with the service running and when restored in a specific environment it will need to respond effectively.   This is quite different than when we reboot and then run the startup script for the service.    

During the lxc launch or lxc start process we must be able to receive the critical variables like "environment variable" and use it to pull new values from the configuration store to adjust where the service is attempting to connect with downstream resources.

We can handle this explicitly by sending a service restart to the underlying service such as tomcat or haproxy from the host during the launch process but still looking at a better process.

When we create containers we set the ENV variable to BUILDIMG so the container knows that it is running in an unspecified Environment.  When we launch the container we will change the ENV variable to show the correct environment which should allow it to find the config store for the that environment. 

#### Downstream Ops Support during image Creation

We talked earlier about creating a set of images by chaining from one to another.  The need to support downstream ops tools such as Nagios for monitoring or making calls out to register the running image in a Inventory manager.  This requires that we also support calling out to scripts during any step of the image creation process to customize a current image with additional functionality.

An example of this is that we know all servers need to be registered with Nagios and we know this must happen when they are launched  but we do not want to add the Nagios code early in the process because when there is a failure we want to be able to test the rest of the image in isolation. 

We could handle this by adding these pipelines late in the stage as nearly the last step in the image creation but it is better to have the flexibility to add them earlier when needed.    This can make it easier to add Nagios configuration  for both the Ubuntu OS and basic tomcat and even add in specific configuration for the set of WAR.

### Mapping hardware host images servers into an environment

todo

### Deploying Container Images

The process of deploying container images for a given environment is designed to be isolated as much as possible from container creation.  There are a few instances when the container it self may need to contain knowledge about what has been deployed.   For instance a haproxy instance needs to know what servers to rout requests to and on which port to send each request.   Wherever possible we will have the core container and then pull any specific things like the Haproxy config file at runtime from a configuration store.   The name of the configuration store and the environment name is set using the lxc environment command options. 

To deploy containers we need the following:

* A list of security zones

  * A list of which security zones are allowed to co-exist on same hardware
  * A list of which security zones are allowed to talk to other zones. 

* A list of servers targeted for deployment

  * A vlan or network security segment for each server

* A list of images to deploy

  * The number of times to deploy each image.

  * The targeted security segment for each image

  * Ports the image needs forwarded to host

  * Maximum Ram consumed by the service

  * Maximum CPU consumed by the service

  * URI prefix used to route layer 7 traffic to that service

  * scripts to run to restart the service

  * 


The system will do the following automatically following

* Based on number of images needed and stated resource usage it will spread them out across the available hardware.
* Will choose an available port to route traffic for each container.
* Will launch the named image on each of the target servers.
* Will save the  port mapping and image placement in well known place for future use.
* Will save the images 

Limiting Assumptions

* The deploy service does not have to be HA because it can be reconstructed or relaunched from a saved image.
* It is OK to publish configuration store content during the start environment process. 

TODO:

* Figure out how we will map security zones to IP addresses.  I think we want one separate IP address per host per security zone. 
* Research using nested containers to allow each security zone to be isolated behind it's own container barrier. 
* 





### Connecting to Operational Infrastructure

TODO: 

### Supporting Version Releases

TODO: 

### Operational Support of Container images





### Keystore major branches

Remote Keystore has several related but discrete major trees from a DevOps Perspective.   It is helpful to model these with different prefixes to avoid name space pollution and to help those browsing the config keystore find what they need.

* **CI/CD build** - Configuration values used to control the build process.  This is typically considered part of the ci/cd process.  In the Java world it would be the work to produce a deployable JAR. 
* **Image Creation** - Configuration values used to control the container image creation process.  These are used to drive the scripts needed create container images we can boot latter.
* **Environment Deploy** - Configuration values used to control where we boot the containers,  How we configure them for port forwarding,   How they are referenced in the layer7 proxy.   How we balance which hosts they will be deployed on.
* **Runtime Control** - Configuration values used to control the software at runtime.  Each service will likely have some service specific values and some that are specific to the service.   Our goal with
* **Operational Control** - Configuration values used to support the operations team such as those used for Nagios configuration.  This one may overlap with some of the other layers.



# Creating Containers





# Creating Container Images

The image creation system is based on the theory that it is faster and less error prone to create images in layers where you have a general updated ubuntu which is the foundation hardened ubunu which is the foundation for basic apache which is the foundation for our application specific apache such as our Key/Value config store.     

If the script to create a given image is ran and a image it depends on does not exist then it will run the script to create it working backwards as far as necessary.  The system is designed to be extensible to so each script only needs to know what template image it starts with and which script to run to create the template when it is missing.   This makes it easy to extend the system to cover a large number of unique images.

### Generic Placeholder Images

We work from an assumption that it is better to wire a entire framework together so it can work end to end as early as possible and then go back and fill in some of the details.   To support this in some instances where we know we will need images such as hardened ubuntu to build other images on we will create a script to create it even if all the work it would normally do is missing. This allows desired hierarchy of images to be modeled early and go back and fill in details without re-engineering.

### Effect on Patching and Updates

This system is based on the theory that we should never patch images but rather we rebuild them from their template images and  re-deploy new containers based on the new images.   It is optimized to support this by  isolating the image creation process from the container launch and container management process.  

To produce new versions of images you can guarantee are fully patched and 100% consistent all you need to do  is remove the existing images and run the image creation scripts for the images you need.   If you run the [make_all_img.sh](../scripts/image/produce/make_all_img.sh) script it will recreate all the images in the proper order to ensure a full set of replacement images become available. 

Once the new images are created then we provide other scripts to manage deploying them to the runtime environment including the ability to support rolling deploys. 

### Make All standard Images

```bash
# assuming that your CWD is where you downloaded
# the repository.  And assuming your lxd init has been
# completed successfully 
 
# Change CWD to where the sample script is located
# just to save some typing.
cd /scripts/image/produce/

bash make_all_img.sh
# This will take a while as it may need to download
# a considerable amount of software.

# This will create a new set of images 
lxc image list

```



### Built in Container Image creation scripts  

* [make_all_img.sh](../scripts/image/produce/make_all_img.sh) - Produce images by running all the image creation scripts listed below. 
* [produce_updatedUbuntu.sh](..//scripts/image/produce/produce_updatedUbuntu.sh) - Starts with a basic ubuntu 18.04,  updates all the packages,  adds a few tools we need in all images. 
* [produce_hardenedUbuntu.sh](../scripts/image/produce/produce_hardenedUbuntu.sh) - Placeholder images starts with updated ubuntu and produces a hardened image for the OS.  This is generally considered desirable practice for each unique OS used.
* [produce_basicApache.sh](../scripts/image/produce/produce_basicApache.sh) - Installs the basic apache server and verifies it is listening on port 80.  Starts with hardened ubuntu.
* [produce_basicHaproxy.sh](..//scripts/image/produce/produce_basicHaproxy.sh) - Installs the basic Haproxy software but does not define any load balancing config.   Starts with hardened ubuntu.
* [produce_basicJDK.sh](../scripts/image/produce/produce_basicJDK.sh) - Installs the default JDK  and starts with hardened Ubuntu.   Used as the base image for all images that need a JDK.  This one takes a long time so it shows the benefits of not having to install it again for derived images.
* [produce_basicTomcat.sh](../scripts/image/produce/.produce_basicTomcat.sh) - Installs the basic Tomcat server and tests to see if it answers on port 8080.   Starts with basic JDK. 
* [apacheKV.sh](../scripts/image/produce/apacheKV.sh)- This script is used to create a application specific Key Value server image.  Starts with basic Apache.   Unlike many image creation scripts this one requires that you supply the location of a Key Value directory structure it can use as content and the suffix you wish to use to name the new container image.
*  [produce_apache_loaded_with_kv-sample.sh](../example/kv-sample/produce_apache_loaded_with_kv-sample.sh). - Produces a ApacheKV image loaded with application specific content.  

### Using a Image creation script create an image



### Creating Application Specific Container Images

One of the application specific containers we wanted was a simple apache that was serving up Values for named keys.  This was intended to act as a simple configuration properties system.   

We have a script to produce a generic apache container image.  That image is extended  by another [apacheKV script](../scripts/image/produce/apacheKV.sh).   The apache KV script needs some information such as the source for where you have stored the [KV tree](../example/kv-sample/kv) you want to copy into the image.   These parameters are supplied by a [example script](../example/kv-sample/produce_apache_loaded_with_kv-sample.sh).   You can create as many different KV store images as desired simply by creating the [directory tree](../example/kv-sample/kv) with files containing the values desired.  

Take a look at the [example script](../example/kv-sample/produce_apache_loaded_with_kv-sample.sh) as it gives you a good idea of how to interact with the rest of the system. 

```sh
 # assuming that your CWD is where you downloaded
 # the repository.  And assuming your lxd init has been
 # completed successfully 
 
 # Change CWD to where the sample script is located
 # just to save some typing.
 cd /devops_lxd_containers/example/kv-sample
 
 # Run the script to create the sample KV image
 bash  produce_apache_loaded_with_kv-sample.sh
```

Once this script completes you should have a image named   apacheKVkvsample You an confirm this with 

```sh
lxc image list  apacheKVkvsample
```

The image creation scripts clean up after themselves and remove the running containers they used.  To test the new container you will  need to launch a new copy.    To launch the container you can use the following command.

```sh
# Launches a new container named test99
lxc launch apacheKVkvsample test99
```

 To test the container and see that the values from our Key Value tree are accessible we can use curl but we need to find out  our containers IP address first.

```sh
# Lookup our IP Address 
# It generally takes lxc 3 to 15 seconds to assign a 
# container a IPV4 address.
lxc info test99
```

Assuming that your IP address from lxc info is 10.213.98.99 your curl command to query the KeyValue store looks as follows:

```sh
# Replace the IP address with the one your image
# was assigned. 
curl -sb -H 10.213.98.99/kv/build/blog/remote_server
# should return something similar to
# http://joeellsworth.com/blog/c198287
```

We also supply a [get_container_ipv4.sh](../../scripts/container/get_container_ipv4.sh), a utility script that gives you a containers IP all parsed out and ready to use in scripts.

```sh
# Assuming that you are in the same CWD from earlier steps 
cd ../../scripts/container
bash get_container_ipv4.sh test99

# You can assign this to a variable using the 
# following bash command.
cip=$(bash get_container_ipv4.sh test99)
echo "cip: $cip"

# This allows you to use Curl without retyping the
# IP address
curl -sb -H $cip/kv/build/blog/remote_server
```

# Running Containers

TODO: 

# Mapping Containers to Ports on Host

TODO: 

# Deploying Containers to Multiple Hosts

TODO: 

# Choosing deployment targets for various containers

TODO: 

# Adding Support for Monitoring 

TODO: 

# Adding support for Inventory Management

TODO: 







# Known Images Needed in our Basic Environment

TODO:



# Isolating Containers between Environments 

One of the rules when building environments is that we want to create conditions whenever possible so services running in one environment are physically unable to call services deployed for another  environment.    OpenStack provides a nice abstraction called a project to deliver this kind of isolation which we can use but we also want this design to provide adequate isolation when using Linux on Bare Metal,   Linux on VMWare and Linux on small BRIX style compute units.      We want to keep as much flexibility in placement of work loads as possible while minimizing  the risk of undesirable interaction from one environment to another.  For cost control reasons it is desirable to be able to  place workloads from many environments on a single host while also being able to easily restrict this so the containers are never mixed.   If done correctly it really should not matter where the Linux container is running and we should be able to easily MIX hosts capable of running LXD regaurdless of what they are hosted on. 

One approach to isolation is to simply place a small number of compute Nodes in each network zone for each environment.  It is also desirable at the network level to isolate calls so  containers  running in one security zone can only access via the network hosts in security zones where communication is allowed.    This desire for isolation competes with the desire to minimize cost,   maximize flexibility and to eliminate human involvement whenever possible.  

to minimize human involvement for configuration and the desire to allow maximum flexibility when placing work loads to maximize 

We provide basic basic isolation services from our software infrastructure.  We also assume that in most corporations the infrastructure team will not trust this level of isolation and will require  that we install specific nodes in different network security zones where they are able to supply the appropriate IP address and control traffic on the desired ports.  Our approach allows both to peacefully co-exist even though the network isolation layer may be redundant. 

- Each environment ends up with it's hosts mapped to different layer 4 ports on each host  even when sharing the same hosts.      

- The container ports mapped to expose on each host is never published because it changes every time a container is launched or updated.

- Web Tier traffic is automatically mapped by a Layer 7 router which knows the current port mapping direct traffic to individual containers on individual ports is never seen. 

- All Traffic into containers is only allowed as explicitly allowed all other is rejected except from specific hosts. 

  - IP level filtering for Web tier filtering is applied at the container level.  Only  traffic from the hosts where the system has deployed the layer 7 ingress proxy is allowed.

  - For containers that must work as a cluster on multiple hosts we automatically configure and limit access on those ports to only the cluster members.   

    This does not guarantee other environments could not access them if the same container such as Kafka is deployed on the same host for multiple environments but since the port changes for each container launched there is some isolation by obscurity.   We can not use the environment level IP level filtering even though each container get's it's own internal IP address because we are exposing the cluster members via port mapping to allow us to keep each cluster member on different hosts for performance and availability.    We automatically configure the usage of these ports so we have a high degree of confidence of no cross talk but for V1 we will simply choose to place those loads on different hosts to allow us to expose the services on the well known ports. 

  - For containers that must be exposed as layer 4 services expose them via a Layer 4 proxy that can be mapped to a well known DNS mapped name and the individual containers use IP level filtering to reject traffic from all other sources.  is proxy is exposed as a environment specific name such as  UAT.ELK.ABC.COM   

    This Layer for proxy can not be used for Kafka or Zookeeper where individual clients expect to be able to talk directly to the cluster member.   This pattern is avoided wherever possible but some great software like Kakfa still need the capability. 

  - Layer 7 web prefix ports are only known within an environment and only knows about the containers it spawned so it nicely isolates all the web services being managed so none of the detailed routing needs to be exposed beyond the layer 7 proxy. 

- We assume Layer 4 routing at edge of firewall can route traffic for an environment to the appropriate ingress proxy.  

- 

In our configuration files we assume host names, container names and VIPS include a environment string that is expanded to make those names unique in the environment.   When hosts are shared between environments this is not done.  

- We assume that all keys used in the KV config store include a environment name segment to make them unique between environments.   We supply interpolation features so you can define the values one and have the environment names substituted during deployment to absolutely minimize restatement of similar values.   

  This is done using the notion of --ENV-- in the configuration strings and keys.    The system interpolates any value surrounded by "--" such as --KEYNAME-- and substitutes that value with the value currently that environment variable.    And example of this would be a key  --ENV--/searchvip/uri would be expanded so the server is actually looking in for the config space UAT/searchvip/uri.  This expansion is done at boot and is ideally refreshed 

​    



# Setting up the Cluster



One of the primary goals of infrastructure as code is that routing work for computers between environments should be nearly transparent with zero human touches.  This includes adding new nodes to the work cluster.   Network configuration, etc.  

In an ideal scenario the only thing that would have to happen is for a newly booted compute node to start participating is to tell the work controller the IP address, security zone and availability zone.    In reality there are a few configuration items that need to be manually setup.  This section describes the approach I used when using a set of small compute appliances called BRIX as both worker nodes and compute nodes.

There are a few things I have not figure out how to fully automate when using individual computers.    Some of these can be automated better using Open stack or MAAS but I will document the basic steps assuming they are not being used.

## What is a Work Controller

A work controller is composed of one or more nodes that is able to instruct worker nodes (Hosts) to launch, start, stop, delete containers and to map ports for various templates.   It is also responsible to ensure the environment on each host where it allocates works is properly setup to provide the resources needed by the templates allocated in that zone.       Worker nodes generally have this repository cloned and built. 

A Linux instance that is allowing a work controller to deploy work is called a worker Node.    A Work controller is normally also a worker Node because the work controller function is light weight and it would waste resources to run as it's own VM or own Brick.

We generally deploy at least 2 work controllers to allow redundancy.  Due to the low failure rate transition from master or lead server to a backup is currently manual but the transition is fast and easy.     

We use rsync to copy all configuration changes from the primary server to the backup servers once every few minutes.  There can be as many backup servers as desired.  The list of those servers is stored in a config file that is easily edited and managed via version control. 

Every time the master knows that it has written something the backup needs to know about it initiates the rsync early.    During failure conditions you simply login to the backup and run a single shell script to tell it to assume the active role.   It will then take over duties from the master. 



### Controller to Work Node Host Communications

Most of the communication between work controller and worker nodes is done using LXC commands but there are a small number of instances where the controller requires SCP and SSH access to allow local configuration.  

Examples are that it needs to be able to configure the owner of a directory where the container running Kafka is allowed to save it's data.  This command must be ran on the host because LXD does not provide access to the remote host file system directly.    We may also use this access to setup certain host access,  routing,  monitoring and logging at the host level. 



## Worker Node (host) Setup

These steps are easily done by hand using the default Ubuntu install.  On our BRIX compute devices it consumes about 10 minutes total when using 1T SSD or NVME install devices.

*  One of the hardest things to automate is the configuration of Network unless you make the a simplifying assumption like we do that we accept the DHCP config on the primary ethernet and then use our router config to permanently lock that MAC address of our primary controller to it's initial IP addressed assigned by IP. 
* The other hard thing to automate is the Disk configuration.  It really is a site specific choice about how to configure the disk drives of the system.   For our Brix appliances We choose to use LVM to manage our primary boot SSD and then we allocate our NVME storage as a device for use by LXD.  That way our work load containers get the fast storage by default.  If any one container requires it's own block storage we allocate it LVM and attach it using LXD device commands.

### Initializing Worker Nodes (hosts)

TODO: Show basic hosts_prep/ubuntu setup script

TODO:  Add link to basic LXD Yaml

TODO: Explain how Yaml changes or LXD Init questions change 

TODO:  How how to use logical volume manager to create a black storage device to use for the LXD storage device.

TODO: How to use Logical volume manager to create a mountable device for use by Kafka rather than having it use a host sub directory.

TODO:   Show the Cloud Init used to setup the SSH so we can run the LXD Init Method SSH Connection 

### Allowing SSH Access to Worker Nodes (hosts)

TODO:  Show the SSH configuration to allow remote work control node to access a host via SSH without requiring a login prompt.     

#### Restricting SSH Access to only Work Controllers

 TODO: Also show modifying hosts.allow to only allow the work controllers access  on port 22 and reject all other users.

#### Setting up Ubuntu Image to do all this initialization Automatically

TODO:  Show how to create a Ubuntu Boot image that does all this automatically or some other process to automatically do this setup.  https://help.ubuntu.com/lts/installation-guide/i386/ch04s06.html 

### Securing & Locking down control node / worker nodes (hosts)

TODO:  Explain the locking down of the LXD init to remove the remote attach once a node has associated by the worker node.

### Determining which nodes can be used for each Environment

TODO: Explain the Environments[] array in the hosts system. 

 



## Connecting Nodes to the Work Controllers

Once we boot each compute node and get it initialized we still must configure it so our work controller nodes can interact with them, launch images,  delete images, etc.   

### LXD Connecting Work Nodes to Work Controller

TODO: Show the LXD Attach command explain it in the context of our sample network.  

This is one of the most obnoxious things we have found so far because the LXD attach command requires a human to respond.   We will eventually figure out how to bypass this command so we can script this from the list of IP addresses.



### Allowing SSH Access from Work Controller to Work Nodes

Some

TODO: Show how to setup SSH access from Work Controller without requiring login.  While restricting it so only the work controllers are allowed access to the host on port 22. 



## Isolating Nodes between Environment





## Placing Nodes in proper network segment



### Adding OpenStack VM 

OpenStack provides a number of primitives that make it relatively easy to fully automate adding VM running Linux to our LXD work cluster.    We can have these VM's provisioned in any Openstack cluster and can mix hosts from VMWare,  Bare Metal Linux and Openstack and it will work transparently provided they are configured to to participate. 

The basic addition and configuration process for a VM running on Openstack is nearly identical to that used to provision a Bare Metal compute prick but Openstack provides us some additional features that allow us to 

TODO: Show how to use cloud init to minimize manual configuration steps getting a node registered.  

TODO: Show how to use cloud init to

