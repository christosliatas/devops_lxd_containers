example/config1/readme.txt

Configuration Directory Structure:
  When we name a directory that contains a configuration we can
  actually name several directories. The system will always 
  process the directories in the order presented it can overwrite
  any values for the same files.  If the file ends with a simple
  .json then it's contents will directly overlay the contents
  of the existing file.  In addition values from any previously 
  set enviornment variables can be used to overlay any
  value in any configuration file.   To maintain compatibility
  with .json we will use a different semantic of --ENVVAR-- where
  this is replaced at the file level with the values defined in 
  the local enviornment files.  When it ends with .ovr.json then 
  we will only update the local settings that have changed in the
  enviornment. Variables are expanded to a full key value to make
  replacement easy for example {start : [ { template: "someval"}]}
  would be expanded to start.XX0.template=someval. This allows 
  fast and easy override of individual files while making it easy
  to support the overlay concept and also makes it easier to 
  save and retrieve values in a remote config store. one intent of
  this expansion is to make it easy to publish some or all of 
  these values to enviornment variables where they can be shared
  across scripts.
  

NOTE: One container we plan to change at runtime without 
  a redeploy is the haproxy layer 7 router.  This is because we
  want to be able to add new nodes with newer image build times
  add them to the load balancer config and then restart haproxy
  haproxy has a feature to allow the new instance to only handle
  new requests while the old version continues to run until it
  all requsts have been completed.  We want to use this feature
  which requires that we re-deploy the proxy config without
  restarting the container.
  
----------------
-- Worflow  ----
----------------
Assuming that all worker template used to boot these images
have been pre-created.

Initial deploy
   1) Load the Configurations
   2) Apply any enviornment specific customization
   3) Figure out work load & Routing from  group_config
      this also includes computing the layer-4 port routing
      to individual containers. 
   4) Generate the Initial Layer 7 routing file 
   5) Produce new haproxy template loaded with the initial routing.
   6) Launch all the requested images. Generate shell to launch 
      all required loads and port maps.  Includes extra step
      to change enviornment variables in host to reflect the 
      enviornment string it is running in and restarting services
      as needed to pick them up.
   7) Expose the required ports via layer-4 port routing
   8) Launch the haproxy configuration delivering traffic to 
      each container.
   9) Test each kv prefix to see if we get predictable results.
  10) Hookup monitoring to allow testing of each container. 
      Includes generating a script to call each VIP and each
      individual containers exposed ports for rational value
      and return error if fails.
  11) Test RAM,CPU, DISK on each avaialble worker node and 
      record so we can use this in the future.

  Includes remembering how we created the enviornment.
  
  Includes remembering ports used so we can find one that
    is available the next time we need one.
     

Maintenance new Release
  1) Figure out which images have changed in some way since
     last full deploy.
     
  2) Create a new image for each image that needs it.
  
  3) Deploy new image to each availability zone along side
     each old image.
     
  4) Test new image to return sane results from set query.
     if sucess then continue else go back and start over
     
  5) Update proxy configuration file with new LB spec to 
     include new image 
     
  6) Deploy new configuration file to the LB and restart
     L7 proxy in way that does not stop any in flight 
     request.
 
  
-----------------
-- FILES --------
-----------------

- - - - - - - - - - - - -
server_list.json
- - - - - - - - - - - - -
  contains the list of servers we have available to load tasks
  onto. This file is the primary file that changes between
  enviornments since the set of servers used by enviornment
  is the primary thing that changes.  
  
  Multiple Environments on same Host:
    It is possible to name the same servers to be used to
    containers from different enviornments but care must be used 
    to ensure port conflicts do not occur.  Port conflicts can be
    avoided by setting the port range listenPortRange different 
    for each enviornment but exra effort is required for ports 
    that can not be layer 7 balanced like ssh.
  
  Data Structure:
    serverName - canonical name we use elswhere in the system 
    to refer to this server.  Normally also mapped to DNS. Acts 
    as key for server object it describes.
      
      .ip: Primary IP this server is known to listen on.
      
      .active: true if server is availabl to allocate work 
         loads
         
      .secZone: Security zones the work planner is allowed to
         allocate work to.   secZone is defined for each image
         template in template_info.json.  These secZone must
         match exactly including case. Each SecZone may require
         it's own L7 proxy to minimize avoidable cross zone
         traffic.
         
      .availZone: The named availability zone this server is
         considered a member of.  This is used to determine
         which templates can be configured to this server.
         The security zone must match spelling exactly.
         the special value "any" allows any workload to be
         deployed on this server regaurdless of what zone
         is specified for the template. 
         
    TODO: Get Number Cores,
      Amount of Ram and available
      disk direct from remote server.
    TODO: Add feature to name where
      ssh key to connect to server can be obtained.
  
- - - - - - - - - - - - -  
template_info.json
- - - - - - - - - - - - -
  contains information needed map the ports the container listens 
  on to an approapriate host port
  
  For some port like http we must stay on the same
  host ports so we can connect with SSH.
  For layer 7 routed http we only track the container
  listen port because we expect the system to track 
  which ports are available to use out of a range 
  of re-usable orts. 
  
  uriPrefix when not specified will 
  result in the sytem not setting up any layer 7 
  load balancing but it will still expose that port on the 
  host.  Since There is only one host port available 
  per host at that name we will be unable to map 
  more than one container of the same image name to 
  that port.  This is an explict tradeoff so we can
  say this image the port will always map to a well
  known port.  It is still possible to do this with 
  the lower level utilities.
  
  For those templates that contain more than a urlPrefix
  then we can setup a layer 7 on behalf of the container.
  and will choose a port # from an available range. This
  allows us to boot new containers on different ports 
  add them to the l7 proxy and let the old traffic 
  bleed off until the old containers are idle after 
  which we can kill them on the host. 
  
   fields
     Key: templateName: - Name of a template used as object
       key. Image template to boot the container from.

       
     secZone: Security zone the container should 
       in.  System will not deploy systems that do not have a 
       matching security zone. 
     
     waitBeforeKill - Number of seconds to give a container
       to bleed off connections after it is been removed from
       the active connection pool before we can kill it. 
     
     produceScript: Script to run to create the image template if
       the system is unable to find a template matching 
       templateName
      
       
     postStartScript: Script to run after the container
        has started.
       
     ports: Array of port mapping used to enable both
        layer 4 routing to expose the container port 
        on the desired host port.   And to latter setup
        layer 7 proxy.
        
        contPort: The port # the container will be
          listening on.
          
        L7: true Will be added to layer 7 proxy  otherwise will
           only provide layer 4 port mapping.
       
        hostPort: Port number on the host the container port
          will be mapped to.
          
        prot: type of connection this map is used for.
          only used fro documenation.   http is for both
          http and https. 
        
         
- - - - - - - - - - - - -
node_map.json
- - - - - - - - - - - - -
  Tells the system which template images to start on 
  which servers by name.  Template image name is used
  in conjuction with the file to determine which host ports
  to map the server to.  
  
  This file is generated from the group_config.json
  file and should not be directly edited.   It is 
  used as a intermediate artifact to allow re-deployments
  of during rolling deploy while keeping the work
  allocation between servers the same.  It will be updated
  by the work planner based on resource consumption,
  security zones and availability zones. 
  
  This version file represents a mid-state data structure that 
  can be generated by the automatic capacity manager. 

  Assumes the system will choose the next available port in 
  listenPortRange and assign it to expose the server port. 
  This is to allow us to spin up new containers forward them
  to a port on the host interface add that port to the layer
  7 routing and then restart haproxy.  This will allow the old
  container to stop receiving new requests while draining off 
  the currently active requests for a zero outage deploy.  We 
  can then simply kill the old containers latter when they have
  no active connections pending. 
  
  FIELDS:
    start: List of templates to start as part of this enviornment. 
    
      template: Image template to boot the container from.
        System will attempt to find a saved alias that 
        matches this name.
           
      contName: Name to give the container in the server
        this name may be adjusted with a count or other
        modifier to ensure uniqueness
        
      postStartScript: Script to run after the container
        has started.
        
      servers: A list of servers to spin the container
        up on.
        
        name: Name of server must match a name in 
          in server_list.
        
        ip:  IP Address of server used in layer 7
          routing. 
        
        ports: Array of port mapping used to enable both
          layer 4 routing to expose the container port 
          on the desired host port.   And to latter setup
          layer 7 proxy. Ports were moved inside the 
          the attributes for each server because the 
          port number mapping will vary by server due to 
          dynamic port assignment.
                   
            contPort: The port # the container will be
              listening on.
              
            hostPort: Port on the host the container port
              will be mapped using layer 4 port mapping.              
            
            uriPrefix: Uri Prefix for matching Layer 7 routing 
              rules.  When not set then no layer 7 routing will
              be established for this port and only the layer 4
              routing will be setup.
                        
             
  WARNING: See description in group config.  nodemap has been
    converted to a generated artifact the system can use latter 
    to support re-deployment with all other factors held equal. 
    The deployment configurator will determine distribution of
    containers across machines and update nodemap.
  
- - - - - - - - - - - - -  
group_config.json
- - - - - - - - - - - - -
  Group config describes the workloads we want to boot,
  how many we want to run,  how they must be spread across
  availability zones.   Security zone limits come from 
  template_info.json.
  
  fields:
    start: Array of templates to start as part of this enviornment. 
    
      template: Image template to boot the container from.
      
      numReq: Number of containers requested. The system will 
        attempt to start the number of containers requested across
        hosts that match the avaialbility zone.
      
      minAvailZone: Number of availability zones the containers
        should be spread across when choosing servers to deploy
        the container on.  This number should be no greater than
        number numReq. 
         
      contName: Name to give the container in the server
        this name may be adjusted with a count or other
        modifier to ensure uniqueness
        
      postStartScript: Script to run after the container
        has started.
       
         
  It is wrong to map server names directly to the nodemap 
  for what we want to boot.  This will cause it to meet our
  zero bit goals because we need to be able to spin up a new
  enviornment and basically only need to change the number 
  of servers to be booted for each primary applications.  
  Instead we should be specifying the servers as belonging 
  to a security zone and an availability zone.  When we 
  specify the boot we should state that we want X instances 
  in security zone Y spread across at least Z availability 
  zones.  We will also eventually need to have a notion of 
  server capacity in memory, cores, disk and a cost per 
  Template instance booted so we can spread the load evenly 
  across the system.  
  
  
  
  
  
