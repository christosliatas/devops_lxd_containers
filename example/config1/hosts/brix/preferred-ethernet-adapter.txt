# List preferred network adapters in order to try to obtain
# current IP address from. In this sample we prefer the
# ethernet cable if it does not have a IPV4 address then
# we will accept the Wifi adapter.
enp3s0
wlp2s0
