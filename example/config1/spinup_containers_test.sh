#!/bin/bash
# Demonstrate how to invoke spinup containers
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
baseDir="$scdir/../.."
echo "scdir=$scdir"
echo "***"
echo "baseDir=$baseDir"
echo "***"
#python spinup_containers.py $baseDir/example/config1 /example/config1/wrktmp  $baseDir 
python spinup_containers.py . ./wrktmp  $baseDir 
