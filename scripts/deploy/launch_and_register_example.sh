#!/bin/bash
# scripts/deploy/launch_and_register_test.sh
# Shows how to operate the launch_and_register.sh script
# to create a VM from a named image map it's ports so they
# can be accessible outside the host. 
#
scriptName="$0"
# Directory where this script exists
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptBase="$scdir/.."
exampleBase="$scriptBase/../example"

bash $scdir/launch_and_register.sh  apacheKVkvsample   apacheKVkvsample01  "NONE:8008=22,/kvsimp:8009=80" mapped.ip.txt noscript
