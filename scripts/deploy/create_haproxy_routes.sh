#!/bin/bash
#scripts/deploy/create_haproxy_routes.sh
# Read the map file specified and write a new file 
# in the form of a haproxy spec to forward different 
# ports on single input listener to different containers
# based on a route prefix string.
#
# hint: run launch_register_several.sh before 
#   running this script so you have valid data
#   in the mapping file
#
# Note: the serverPort specified in the mapping file
#   is unimportant in this context
#   because we are handeling the basic layer 4 routing
#   using lxd device maps.  We are only worried about
#   routing a single listener from a given server port
#   to the containers based on a server IP.


scriptName="$0"
scriptDir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
inName=$1
serverPort=$2
outName=$3
undsc="_"
echo "$scriptName dir: $scriptDir  inpName: $inName  outName: $outName"
if [ "$#" -ne "3" ]; then
  msg='ABORT:  invalid number of aurguments
  
  [1]- Name of input mapping file.  This file is normally
       created by launch_and_register.sh
       
  [2]- serverPort - This is the port ha proxy will be 
       configured to listen on. 
  
  [3]- Name of output file 
  
         
  returns 0 response code if sucess otherwise 1
       
  Example: 
     bash create_haproxy_routes.sh mapped.ip.txt 8083 proxy.routes.txt 
  '
  # TODO: Expand the string to interpolate variable values
  # Send msg to stderr  
  1>&2 echo "$msg"
  exit 1
fi


if [ -e "$inName" ]
then
    echo "inName: $inName does exist"
else
    echo "ABORT: $scriptName  inName: $inName does not exist"
fi

# Example of extracting a unique list of container names
# from the input file.
uniqCont=$(cat $inName | cut -d"," -f1 | sort | uniq | cut -d"=" -f2)
echo "uniqCont: $uniqCont ***"
uniqPrefix=$(cat $inName | cut -d"," -f2 | sort | uniq | cut -d"=" -f2 | sort)
echo "uniqPrefix: $uniqPrefix ***"

echo -e "frontend http_front" > $outName
echo -e "\tbind *:$serverPort" >> $outName
echo -e "\tstats uri /haproxy?stats" >> $outName

  
# Generate the ACL command lines
for apref in $uniqPrefix; do
  if [ "$apref" != "NONE" ]; then
    # strip leading / and convert all other / to _
    nmPref=$(echo $apref | cut -c2- | sed 's/\//_/g')
    echo -e "apref: $apref nmPref: $nmPref"
    echo -e "\tacl url_$nmPref path_beg $apref" >> $outName
    echo -e "\tuse_backend back_$nmPref if url$undsc$nmPref" >> $outName
  fi
done 
echo "" >> $outName 
#echo "  default_backend default_http_back" >> $outName
# The Default Backend is where everything that
# is no routed gets sent. But we do not really 
# want that in this case. 
#echo '
#backend default_http_back
#  balance roundrobin
#  server test 10.41.16.159:8080 check
#  # Add more servers here for the default back end.
#' >> $outName


# Generate the backend for each server. 
for apref in $uniqPrefix; do
  if [ "$apref" != "NONE" ]; then
    # strip leading / and convert all other / to _
    nmPref=$(echo $apref | cut -c2- | sed 's/\//_/g')
    echo -e "L131: apref: $apref nmPref=$nmPref"
    echo -e "backend back_$nmPref" >> $outName
    echo -e "\tbalance roundrobin" >> $outName
  
    # Get the contName, hostIp and Host Port for
    # route prefixes that match our current prefix    
    hosts=$(cat $inName | grep "routePrefix=$apref" | cut -d"," -f1,3,4 | sort | uniq)        
    for hoststr in $hosts; do
      echo "L154: apref: $apref hoststr: $hoststr"
      contName=$(echo $hoststr | cut -d"," -f1 | cut -d"=" -f2)    
      hostIP=$(echo $hoststr | cut -d"," -f2 | cut -d"=" -f2)
      hostPort=$(echo $hoststr | cut -d"," -f3 | cut -d"=" -f2)
      echo "L158: contName: $contName hostIP: $hostIP hostPort $hostPort"
      svrstr=$(echo -e "\tserver serv$undsc$contName $hostIP:$hostPort")
      echo "L160: svrstr: $svrstr"
      echo -e "$svrstr" >> $outName
    done  
  else
    echo "L121: Skip L7 route due to NONE prefix"
  fi
done 


