#!/bin/bash
# Remove an image of the same name if one already exists
# This is required because we will get an error if it 
# already exists when we create the alias.
publishImg=$1
echo "remove old image if it exists"
trun=$(lxc image list $publishImg)
echo "trun=$trun"
if grep $publishImg <<< $trun
  then 
     echo "Old image has been found.  Removing $publishImg"
     lxc image delete $publishImg
fi
