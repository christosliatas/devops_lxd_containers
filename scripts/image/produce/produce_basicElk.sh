##############
## WIP
##############

#!/bin/bash
# shellcheck disable=SC2034,SC2164

# Produce a basic Kafka image from hardenedUbuntu image..

# Save script Dir so we can make sub script calls relative
# shellcheck disable=SC1091
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptName="$0"
scriptBase="$scdir/../.."
exampleBase="$scriptBase/../example"

imgName=wrkKafka
publishImg=basicKafka
template=basicJDK
templateCreateScript=produce_updatedUbuntu.sh

# shellcheck source=./produce_image_setup.sh
source "$scdir/produce_image_setup.sh"

##############
## START ACTUAL Apache Work 
##############

lxc file push $scdir/in-container/install_kafka.sh  /root/scripts/install_kafka.sh

sudo useradd kafka -m
lxc exec $imgName -- mkdir -p $contTargetScripts

lxc file push $kvSrcDir/../index.txt $imgName/$contWebDir/index.txt