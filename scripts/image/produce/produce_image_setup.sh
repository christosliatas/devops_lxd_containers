# Setup code that is needed for pretty much every image
# this is intended to be sourced so the variables defined
# remain in scope.

# Save script Dir so we can make sub script calls relative
cwd=$(pwd)
scdirI=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
bash $scdirI/../remove_image.sh $publishImg
bash $scdirI/../../container/remove_container.sh $imgName

#echo "cwd: $cwd  scriptDir: $scdirI" # printout where we are running from

# If Template Image does not exist then run 
# script needed to produce it.

# Uses a different name than the scdir because we do not want
# to override the value set in calling script.  Only need to do this
# when sourcing instead of calling
bash $scdirI/../create_image_if_not_exist.sh $template $templateCreateScript
checkImgRes=$?
if [ "$checkImgRes" = "1" ];  then
  echo "ABORT: template not available from $template failed"
  exit 1
fi


# Give proper error message
echo "Launch a container with name of $imgName from $template"
lxc launch $template $imgName
launchOK=$?
echo "launch result: ..$launchOK.."
if [ "$launchOK" = "1" ];  then
  echo "ABORT: launch of $imgName from $template failed"
  exit 1
fi

bash $scdirI/../../container/wait_until_container_has_ip.sh $imgName
bexit=$?
echo "bexit=$bexit"
if [ "$bexit" = "99" ]; then
  echo "$imgName timed out without an IP exit abort"
  exit 99
fi
containerIP=$(bash $scdirI/../../container/get_container_ipv4.sh $imgName)
echo "$imgName container has IP=$containerIP"
#echo "L42: $0 cwd: $cwd  scriptDir: $scdirI" # printout where we are running from