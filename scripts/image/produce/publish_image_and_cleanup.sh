#!/bin/bash
# Publish the image and cleanup as needed
imgName=$1
publishImg=$2
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# Cleans up after installs
lxc exec $imgName -- apt-get autoremove --purge -y
lxc exec $imgName -- apt-get clean -y

echo "Cleanup the image delete old packages"
lxc snapshot $imgName clean


echo "Publish the image $imgName as $publishImg"
# This allows us to use it in the future.
lxc publish $imgName/clean --alias $publishImg


echo "stopping image $imgName"
lxc stop $imgName

echo "delete image $imgName"
lxc delete $imgName

# List the images created on the local host. The image 
# published in prior step should show up in this list.
echo "Published image $publishImg should now be available to launch"
lxc image list $publishImg
