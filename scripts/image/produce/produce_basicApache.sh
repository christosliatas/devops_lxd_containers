#!/bin/bash
# shellcheck disable=SC2034,SC2164
# See: https://github.com/koalaman/shellcheck/wiki/Directive

# Produce a basic tomcat image from hardenedUbuntu image..

# Save script Dir so we can make sub script calls relative
# shellcheck disable=SC1091
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

imgName=wrkApache
publishImg=basicApache
template=hardenedUbuntu
templateCreateScript=produce_updatedUbuntu.sh

# shellcheck source=./produce_image_setup.sh
source "$scdir/produce_image_setup.sh"

##############
## START ACTUAL Apache Work 
##############

lxc exec $imgName -- apt install apache2 --assume-yes

# Test to See if our new Server is responding 
# Replace IP address swith the one reported from
# lxc info tomcat1
curl "$containerIP:80"
# Should see some html markup.


echo "copy a sample war file into our tomcat"
lxc exec $imgName --  wget -O /var/lib/tomcat8/webapps/sample.war  https://tomcat.apache.org/tomcat-8.5-doc/appdev/sample/sample.war

#TODO: Add some content to Apache

#TODO: Add Hardening Here
# Produce a separate version of hardenedApache using the following
# security tips It should be saved as a separate image than basic 
# apache
# See: https://www.tecmint.com/apache-security-tips/
# See: https://geekflare.com/10-best-practices-to-secure-and-harden-your-apache-web-server/
# See: https://geekflare.com/apache-web-server-hardening-security/

bash "$scdir/publish_image_and_cleanup.sh" $imgName $publishImg
