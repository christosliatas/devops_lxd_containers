#!/bin/bash
# Attempt to build all the images we know we may need
# This will build those first in the dependency order
# to ensure all images have the most recent version
# of packages and are fully patched.
#
# NOTE: TOP indicates that the lxc publish takes 100%
#   of a single core while leaving other cores idle I changed
#   this to run many in parrlell using the & operator. This 
#   added the complexity of waiting for some background 
#   processes to finish before dependant task could be 
#   built. 
# 
#  TODO: Setup a proxy to avoid downloading over and over again https://help.ubuntu.com/community/AptGet/Howto 
#
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
bash $scdir/produce_updatedUbuntu.sh
bash $scdir/produce_hardenedUbuntu.sh
bash $scdir/produce_basicApache.sh 
bash $scdir/produce_basicHaproxy.sh 
bash $scdir/produce_basicJDK.sh 
bash $scdir/produce_basicOpenJDK8.sh 
bash $scdir/produce_basicOpenJRE8.sh 

# Must wait until JDK is finished to produce 
# the produce_basicTomcat

echo "produce_basicJDK.sh exit status: $jdkstat"
#TODO:  If JDK fails should abort since the 
#  tomcat will fail anyway.
bash $scdir/produce_basicTomcat.sh 


# the KV sample that is derived from it like our
# KV sample

#TODO: If Apache fails then should abort since the
#  kv-sample and any other derived will fail 

bash $scdir/../../../example/kv-sample/produce_apache_loaded_with_kv-sample.sh 
PidKVSample=$!


bash $scdir/kafka/produce_basicKafka.sh 
PidJOpenJRE=$!

echo "All Builds Finished"

# TODO: Check error codes and abort run if prior package is not sucessful

