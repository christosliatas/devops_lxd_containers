#!/bin/bash
# shellcheck disable=SC2034,SC2164

# Produce a basic Kafka image from hardenedUbuntu image.
# to test this access the container at /home/kafka/kafka and 
# run the script test_kafka  To make this useful you need to 
# Launch the container and then map it to an external port.
#
# Kafka and Zookeeper are not really N+ type configuration 
# because each server in the cluster must know about all 
# other servers.    We want a single binary image for all instances
# so we will set the instanceID when the node is launched and then
# force a script to run that changes the serverName and instance
# ID in the zookeeper config file.  We can do that work in the 
# cloudInit where the istance ID in the enviornment variable 
# is different then change the config file to match.  See: https://www.networkworld.com/article/2693438/operating-systems/unix-how-to-the-linux-etc-inittab-file.html
# which should check the contents of the ENV and instance ID  
# variables and if they have changed then should update the 
# zookeeper config file and restart both services.
#
# #WIP Making Changes to allow same script to be called multiple
# times for each server in the cluster. 
# arg[1] = Instance ID 
# arg[2]= file containing list of servers in cluster. 
#  Instance ID is used to look up the hostname or 
#  IP of the server in the list of servers in the cluster
# 
# ENV - Enviornment variable to pass as ENV to container.
# KVURI - URI of KV config store to call for other parameters
# LogDestServ - Server to send log data to.


# Save script Dir so we can make sub script calls relative
# shellcheck disable=SC1091
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptName="$0"
scriptBase="$scdir/../.."
exampleBase="$scriptBase/../example"
ENV="BUILDIMG"

imgName=wrkKafka
publishImg=basicKafka
template=basicOpenJRE8
templateCreateScript=$scdir/../produce_basicOpenJRE8.sh


cwd=$(pwd)

# shellcheck source=./produce_image_setup.sh
source "$scdir/../produce_image_setup.sh"

cwd=$(pwd)
echo "L29: cwd: $cwd  scriptDir: $scdir" # printout where we are running from

# Kafka and Zookeeper both need a unqiue ID we can use to uniqely identify each cluster
# member. 
kafakInstanceId=1
if [ "$#" -eq "1" ]; then 
  kafakInstanceId=$1
fi


##############
## START ACTUAL Kafka Work 
##############
lxc config set $imgName environment.ENV "$ENV"
lxc config set $imgName environment.kafkaInstanceId "$kafakInstanceId"
lxc config set $imgName environment.KAFKA_HOME "/home/kafka/kafka"
lxc config set $imgName environment.kafkapass "kafka123"

lxc exec $imgName -- mkdir -p /root/scripts


# Copy our main install script over
lxc file push $scdir/in-container/install_kafka.sh $imgName/root/scripts/install_kafka.sh
lxc file push $scdir/in-container/on-launch.sh $imgName/root/scripts/on-launch.sh

#TODO: Modify the Kafka Config file to point at our desired mount point


#lxc file push $scdir/in-container/kafka-env-var.sh $imgName/etc/profile.d/kafka-env-var.sh
lxc file push $scdir/in-container/install_zookeeper.sh $imgName/root/scripts/install_zookeeper.sh

# Push our our generated config values over to the Kafka server
#lxc file push $scdir/in-container/source-variables.sh $imgName/root/scripts/source-variables.sh # Not needed because using the lxc config setting 

#   TODO: Add Script that produces the source-variables from 
#   config store values rather than a static file.

# Run the Local Actual kafka install script in container.
lxc exec $imgName -- bash /root/scripts/install_kafka.sh 

# Cleanup our image after install some of these values
# could contain insecure files during the install process
#lxc exec $imgName -- rm /root/scripts/source-variables.sh
lxc exec $imgName -- rm /root/scripts/install_kafka.sh
lxc exec $imgName -- rm /root/scripts/install_zookeeper.sh

bash "$scdir/../publish_image_and_cleanup.sh" $imgName $publishImg

echo "Run our Kafka Test "
lxc file push -p $scdir/in-container/test_kafka.sh $imgName/home/kafka/kafka/test_kafka.sh
lxc exec $imgName -- bash /home/kafka/kafka/test_kafka.sh

#TODO:  Test Kafka Python Client https://towardsdatascience.com/kafka-python-explained-in-10-lines-of-code-800e3e07dad1


