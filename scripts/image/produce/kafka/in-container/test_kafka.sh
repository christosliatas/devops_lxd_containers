#!/bin/bash
# Simple test sends some records receive some records 
# from a simple single server Kafka instance 
#
# https://kafka.apache.org/quickstart
# https://kafka.apache.org/21/documentation/streams/quickstart
#
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd $scdir

function timeMS() {
 echo $(date +%s%N | cut -b1-13)
}

echo " tested creation of Hello-Kafka test topic"
sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Hello-Kafka


echo "Send Some data to Kafka"
su -l kafaka

tstr="
Record first
2nd record
3rd record
record x
I can keep going
"

echo "press CTRL-C to stop sending"
echo "$tstr" | bin/kafka-console-producer.sh --broker-list localhost:9092 --topic Hello-Kafka  
sleep 1



echo "Now Read what I sent - press CTL-C to stop listening for messages"
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic Hello-Kafka --from-beginning



