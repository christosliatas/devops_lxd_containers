###############
### WIP
###############
#!/bin/bash
#Install Kafka to run in a container 
# This script works in conjunction with produce_basicKafka
# it is intended to be ran from inside the container. 
#
# See: 
#   https://linuxhint.com/install-apache-kafka-ubuntu/
#   https://www.digitalocean.com/community/tutorials/how-to-install-apache-kafka-on-ubuntu-18-04#step-5-—-testing-the-installation 
# https://www.bogotobogo.com/Hadoop/BigData_hadoop_Zookeeper_Kafka_single_node_single_broker_cluster.php
# as needed. 
#
#set -x

scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# Add the Kafka user and group to control access
groupadd kafka
useradd -m -d /home/kafka -g kafka -G sudo kafka 
echo -e "$kafkapass\n$kafkapass\n" | passwd kafka
echo "L34: KAFKA_HOME=$KAFKA_HOME"

# Setup Data Directories where we will mount the Host File system
# when launching the container.  
mkdir -p /home/kafka/data
mkdir -p /home/kafka/logs
chown -R kafka:kafka /home/kafka

mkdir -p /home/zookeeper/data
mkdir -p /home/zookeeper/logs
chown -R kafka:kafka /home/zookeeper


# Note the enviornment variables set with lxc config are not
# saved through a publish as alias and re-launch os if we want to 
# Keep them we must keep them in the image or reset them after
# the container is relaunched or set them in a profile when the 
# container is launched.

# Save Enviornment variables we want available through the next boot cycle
# No worry about overlaying avariables set even if we duplicate them the 
# last setting wins so we do not have to worry about duplicates except for
# execution time. 
envVars="
export ENV=$ENV
export kafkaInstanceId=$kafkaInstanceId
export KAFKA_HOME=/home/kafka/kafka
"
# Save as separate file for remote login users
echo -e "$envVars" >  /etc/profile.d/kafka_env.sh

# Set the enviornment variables so they are available in every session
# for every user
echo -e "$envVars" >>  /etc/environment
echo -e "Env Vars=$envVars"



# Install Zookeeper needed by Kafka
bash $scdir/install_zookeeper.sh
echo "L38: Finished with Zookeeper"
sleep 5

mkdir -p /home/kafka/downloads
mkdir -p /home/kafka/kafka
mkdir -p /home/kafka/logs
echo "L48: Kafka downloaded"

# Fetch the Kafka binaries.
cd /home/kafka/kafka
# Fetch the remote file
wget http://www-eu.apache.org/dist/kafka/2.1.0/kafka_2.12-2.1.0.tgz
# extract the file

tar -xvzf kafka_2.12-2.1.0.tgz --strip 1
rm kafka_2.12-2.1.0.tgz
echo "L57: Kafka Unziped"

chown -R kafka:kafka *

# Start Zookeeper 
nohup bash bin/zookeeper-server-start.sh config/zookeeper.properties &
sleep 5


# Start Kafka 
nohup bash ./bin/kafka-server-start.sh config/server.properties &
#############
# TODO: Convert this A Service for Kafka
#############
# https://kafka.apache.org/quickstart 
# https://www.bogotobogo.com/Hadoop/BigData_hadoop_Zookeeper_Kafka_single_node_single_broker_cluster.php


sleep 5
netstat -nlpt
echo "L63: Kafka started "

# TODO: Modify Kafka Config As needed here

# Change user so we can run under kafka user

sudo passwd kafka -l
sudo deluser kafka sudo
echo "L72: Kafa user locked for no login"


exit 0 


# TODO: Export Ports for Kafka and Zookeeper so it can be used by clients outside of 