#!/bin/bash
# Produce a basic a Image with the default
# Java Runtime environment installed.  This is
# preferred when you want to run an existing Java
# application but do not need or want the compiler. 

scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
imgName=wrkOpenJRE8
publishImg=basicOpenJRE8
template=hardenedUbuntu
templateCreateScript=$scdir/produce_hardenedUbuntu.sh

source $scdir/produce_image_setup.sh

##############
## START ACTUAL Tomcat WORK HERE
##############

# Install the deafult JDK
lxc exec $imgName -- apt-get update -y
lxc exec $imgName -- apt-get install openjdk-8-jre -y

# TODO: Add Hardening HERE

bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg

