#!/bin/bash
# Produce a basic tomcat image from hardenedUbuntu image..

imgName=wrkTomcat
publishImg=basicTomcat
template=basicJDK
templateCreateScript=$scdir/produce_basicJDK.sh
# Save script Dir so we can make sub script calls relative
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

source $scdir/produce_image_setup.sh


##############
## START ACTUAL Tomcat WORK HERE
##############

# Add the group $imgName
lxc exec $imgName -- groupadd tomcat

# Adds a user tomcat of the group tomcat
lxc exec $imgName -- useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

# Install Tomcat8 
lxc exec $imgName -- apt install tomcat8  --assume-yes

# Start the tomcat service
lxc exec $imgName -- service tomcat8 start


# Test to See if our new Server is responding 
# Replace IP address swith the one reported from
# lxc info tomcat1
curl $containerIP:8080
# Should see some html markup.


echo "copy a sample war file into our tomcat"
lxc exec $imgName --  wget -O /var/lib/tomcat8/webapps/sample.war  https://tomcat.apache.org/tomcat-8.5-doc/appdev/sample/sample.war

sleep 20
echo "restart the Tomcat service after copying in the WAR"
lxc exec tomcat -- service tomcat8 restart

echo "Fetch immediately may fail until tomcat has a chance to recognize the new WAR"
curl $containerIP:8080/sample/index.html
sleep 5

echo "after 5 seconds should have had time to make war contents available"
echo "Try to Fetch index.hml from sample war"
curl $containerIP:8080/sample/index.html
echo ".."
echo "try to fetch  hello.jsp from sample war"
echo ".."
# TEst to see if the dynamic JSP also worked
curl $containerIP:8080/sample/hello.jsp


#TODO: Add Hardening Here
# See: https://www.owasp.org/index.php/Securing_tomcat


bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg
