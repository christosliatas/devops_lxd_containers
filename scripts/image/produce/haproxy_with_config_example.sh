#!/bin/bash
#scripts/deploy/create_haproxy_routes_test.sh
# Run the layer 7 proxy config script for some 
# known sample data.
# Assumes that launch_and_register_several_example.sh ran 
# sucessfully after which create_ha_proxy_routes.sh was ran.
# and that it was able to create the file 
# scripts/deploy/proxy.routes.txt

bash haproxy_with_config.sh rt1 ../../deploy/proxy.routes.txt  /kv/ build 

# $1 - Suffix to add to normal haproxy image name to make
#      it unique 
#
# $2 - file containing generated proxy config file that will
#      be appended to the haproxy configuration.
#
# $3 - uri prefix that will be added to a curl command to ensure
#      the proxy setup worked as expected.
#
# $4 - a string that is expected to be in the curl result used
#      to validate the proxy returned something expected. 
#
