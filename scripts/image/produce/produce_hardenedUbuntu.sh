#!/bin/bash
# Produce a fully hardened Ubuntu image from a fully updated image.

# Save script Dir so we can make sub script calls relative
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

imgName=wrkUbuntu
publishImg=hardenedUbuntu
template=updatedUbuntu
templateCreateScript="$scdir/produce_updatedUbuntu.sh"
source $scdir/produce_image_setup.sh




#######################
### TODO: ADD REAL HARDENING LOGIC HERE
#######################
echo "###################################"
echo "WARNING THIS IS ONLY A PLACE HOLDER FOR A HARDENED IMAGE IT IS NOT REALLY HARDENED"
echo "###################################"


# TODO: Add Hardening Here




bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg

