
#!/bin/bash
# scripts/container/forward_port_host_to_container.sh
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
source $scdir/utility_functions.sh

function removeDuplicateSlash_test() {
  echo "Test removeDuplicateSlash()"
  a="joe//jim/jack///james"
  b=$(removeDuplicateSlash $a)
  echo "b=$b"
  if [ "$b" = "joe/jim/jack/james" ];  then
    echo "SUCCESS: removeDuplicateSlash_test"
    return 0
  else
    echo "FAIL: removeDuplicateSlash_test"
    return 1
  fi
}


function removeDuplicateWhiteSpace_test() {
  echo "test removeDuplicateWhiteSpace()"
  a=$(printf "able joe    jim        jackie\t \n  james")
  b=$(removeDuplicateWhiteSpace "$a")
  echo "b=$b"
  if [ "$b" = "able joe jim jackie james" ];  then
    echo "SUCCESS: removeDuplicateWhiteSpace"
    return 0
  else
    echo "FAIL: removeDuplicateWhiteSpace"
    return 1
  fi
}


function fileNameFromPath_test() {
  echo "test fileNameFromPath()"
  a=$(printf "/devops/scripts/test/fun/index.txt")
  b=$(fileNameFromPath "$a")
  echo "b=$b"
  if [ "$b" = "index.txt" ];  then
    echo "SUCCESS: fileNameFromPath"
    return 0
  else
    echo "FAIL: fileNameFromPath"
    return 1
  fi
}


function normalizePath_test() {
  echo "test normalizePath()"
  a=$(printf "/home/jwork/devops/devops/scripts/test/fun/../../index.txt")
  b=$(normalizePath "$a")
  echo "b=$b"
  if [ "$b" = "/home/jwork/devops/devops/scripts/index.txt" ];  then
    echo "SUCCESS: normalizePath"
    return 0
  else
    echo "FAIL: normalizePath"
    return 1
  fi
}




removeDuplicateSlash_test
removeDuplicateWhiteSpace_test
fileNameFromPath_test
normalizePath_test
