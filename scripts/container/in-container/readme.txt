scripts/container/in-container

Scripts intended to be loaded into the container and
executed in place.

This is needed because some kinds of manipulation 
are much more difficult when done from outside the
container. 
