#!/bin/bash
# Update Existing Packages
imgName=$1
echo " Update existing packages "
lxc exec $imgName -- apt-get update
lxc exec $imgName -- apt-get dist-upgrade -y
