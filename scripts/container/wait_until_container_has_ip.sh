#!/bin/bash
# Wait until the requested container has received an IP address
# or timeout after 40 seconds. Returns error code 99 if fails
# or 1 on success.
imgName=$1
echo "waiting for $imgName to have an IP"
counter=0
for counter in `seq 1 40`;
do
  let counter+=1
  trun=$(lxc list $imgName)
  echo "trun=$trun"
  if grep $1.*RUNNING\.\d*\.\d*\.\d*\.\d*.*eth\d*.*\:.*\:.*\:  <<< $trun
    then 
      echo "Is running"
      exit 1
    else
      echo "waiting for ip to $imgName show in lxc list"
      sleep 1
    fi
done
echo "timeout waiting for $imgName to receive a ip"
exit 99
