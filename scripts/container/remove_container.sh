#!/bin/bash
# Stop and remove container if it exists We need to do this
# because we want a brand new container spawned from then
# specified template.
imgName=$1
echo "stop and delete container if already defined"
trun=$(lxc list $imgName)
echo "trun=$trun"
if grep $imgName <<< $trun
  then 
     echo "Old container $imgName has been found.  Stop and delete $imgName"
     lxc stop $imgName
     echo "delete image $imgName"
     lxc delete $imgName
fi
